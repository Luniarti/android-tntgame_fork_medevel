package de.jaschastarke.android.db;

import java.util.ArrayList;
import java.util.List;

import de.jaschastarke.android.tasks.FixedAsyncLoader;
import android.content.Context;
import android.database.Cursor;

public class DBLoader<T extends DBEntry> extends FixedAsyncLoader<List<T>> {
    private DBHelper db;
    private DBTable<T> table;
    private String where = null;
    private String[] whereArgs = null;
    private String orderBy = null;
    
    public DBLoader(Context context, DBHelper db, DBTable<T> table) {
        super(context);
        this.db = db;
        this.table = table;
    }
    
    public DBLoader<T> setWhere(String where) {
        this.where = where;
        return this;
    }
    
    public DBLoader<T> setWhereArgs(String[] whereArgs) {
        this.whereArgs = whereArgs;
        return this;
    }
    
    public DBLoader<T> setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }
    
    public DBLoader<T> setWhere(String where, String[] whereArgs) {
        setWhere(where);
        setWhereArgs(whereArgs);
        return this;
    }
    

    @Override
    public List<T> loadInBackground() {
        synchronized (db) {
            Cursor c = db.getReadableDatabase().query(table.getName(), table.getColumns(), where, whereArgs, null, null, orderBy);
            List<T> ret = new ArrayList<T>(c.getCount());
            while (c.moveToNext()) {
                ret.add(table.createEntry(c));
            }
            c.close();
            db.close();
            return ret;
        }
    }
}
