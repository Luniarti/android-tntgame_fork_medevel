package de.jaschastarke.android.db;

import android.content.ContentValues;

public interface DBEntry {
    public long getId();
    public DBTable<?> getTable();
    public ContentValues getContentValues();
}
