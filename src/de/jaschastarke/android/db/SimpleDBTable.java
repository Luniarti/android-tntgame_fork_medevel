package de.jaschastarke.android.db;

import android.database.Cursor;

public interface SimpleDBTable {
    public abstract String getName();
    public abstract DBEntry createEntry(Cursor cursor);
    public abstract String[] getColumns();
}
