package de.jaschastarke.android.db;

import android.database.Cursor;
import android.provider.BaseColumns;

public interface DBTable<T extends DBEntry> extends BaseColumns, SimpleDBTable {
    public abstract String getName();
    public abstract T createEntry(Cursor cursor);
    public abstract String[] getColumns();
}
