package de.jaschastarke.android.tasks;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

abstract public class FixedAsyncLoader<D> extends AsyncTaskLoader<D> {
    private D currentData;
    public FixedAsyncLoader(Context context) {
        super(context);
    }
    
    @Override
    protected void onStartLoading() {
        if (currentData != null) {
            deliverResult(currentData);
        } else {
            forceLoad();
        }
    }
    

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(D data) {
        /*if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (data != null) {
                onReleaseResources(data);
            }
        }*/
        //List<T> oldData = data;
        currentData = data;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(data);
        }
        /*
        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldData != null) {
            onReleaseResources(oldData);
        }*/
    }
    @Override
    protected void onReset() {
        super.onReset();
        currentData = null;
    }
}
