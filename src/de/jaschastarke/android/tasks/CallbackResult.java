package de.jaschastarke.android.tasks;

public interface CallbackResult<T> {
    public void onCallback(T result);
}
