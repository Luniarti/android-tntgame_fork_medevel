package de.jaschastarke.android.tasks;

import android.os.AsyncTask;

public abstract class SimpleTask<Params, Result> extends AsyncTask<Params, Void, Result> {
    protected CallbackResult<Result> callback;
    public SimpleTask<Params, Result> setCallback(CallbackResult<Result> cb) {
        callback = cb;
        return this;
    }
    
    @Override
    final protected void onPostExecute(Result result) {
        if (callback != null)
            callback.onCallback(result);
    }
}
