package de.jaschastarke.android.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class SendErrorReportActivity extends Activity {
    private static final String FILTER = "ActivityManager:I AndroidRuntime:E tnt.game:V *:F";
    private File file;
    
    @Override
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Process mLogcatProc = null;
        BufferedReader reader = null;
        try {
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Toast.makeText(this, "SDCard nicht verfügbar", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            File publicDir = Environment.getExternalStorageDirectory();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                publicDir = getExternalFilesDir(null);
            }
            File dir = new File(publicDir, "errorreports");
            dir.mkdirs();
            for (File oldFile : dir.listFiles()) {
                oldFile.delete();
            }
            
            int ts = (int) (System.currentTimeMillis() / 1000);
            file = new File(dir, "log_" + ts + ".txt");
            
            mLogcatProc = Runtime.getRuntime().exec(new String[] {"logcat", "-d", "-v", "threadtime", FILTER});
            reader = new BufferedReader(new InputStreamReader(mLogcatProc.getInputStream()));
            final StringBuilder log = new StringBuilder();
            
            try {
                PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
                log.append("App-Version: ");
                log.append(info.versionName + " (" + info.versionCode + ")");
            } catch (NameNotFoundException e) {
                e.printStackTrace();
                log.append("App-Version: ");
                log.append(e.toString());
            }
            log.append("\nOS Kernel-Version: ");
            log.append(System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")");
            log.append("\nOS Release: ");
            log.append(android.os.Build.VERSION.CODENAME + ", " + android.os.Build.VERSION.INCREMENTAL + ", " + android.os.Build.VERSION.RELEASE);
            log.append("\nOS API Level: ");
            log.append(android.os.Build.VERSION.SDK);
            log.append("\nDevice: ");
            log.append(android.os.Build.DEVICE);
            log.append("\nModel (and Product): ");
            log.append(android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")");
            log.append("\n\n");

            String line;
            String separator = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                log.append(line);
                log.append(separator);
            }
            
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file));
            out.write(log.toString());
            out.close();

            //Uri uri = FileProvider.getUriForFile(this, "de.jaschastarke.android.tools.errorreport.fileprovider", file);
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            //shareIntent.setDataAndType(uri, "text/plain");
            shareIntent.setType("text/x-plain");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"jascha@ja-s.de"});
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Android App Log");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            Intent chooser = Intent.createChooser(shareIntent, "Android App Log send via...");
            chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(chooser, 0);
            finish();
        } catch (IOException e) {
            Log.e("tnt.game.de.jaschastarke.android.tools.Exception", "Failed to read logs", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e("tnt.game.de.jaschastarke.android.tools.Exception", "Failed to read logs", e);
                }
            }
        }
    }
}
