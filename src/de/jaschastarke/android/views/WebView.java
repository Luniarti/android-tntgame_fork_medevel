package de.jaschastarke.android.views;

import android.content.Context;
import android.util.AttributeSet;

public class WebView extends android.webkit.WebView {
    private Runnable initScrollListener;
    
    public WebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public WebView(Context context) {
        super(context);
    }
    
    public void setOnContentVisible(Runnable listener) {
        initScrollListener = listener;
    }
    @Override
    public void invalidate() {
        super.invalidate();
        if (initScrollListener != null && getContentHeight() > 0) {
            initScrollListener.run();
            initScrollListener = null;
        }
    }
}
