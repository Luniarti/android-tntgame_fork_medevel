package de.jaschastarke.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class UpdateBar extends ProgressBar {
    public UpdateBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public UpdateBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public UpdateBar(Context context) {
        super(context);
    }
    
    
}
