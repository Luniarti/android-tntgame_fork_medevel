package de.jaschastarke.utils;

public final class FileUtil {
    private static final char CHAR_PATH_SEP = '/';
    private static final char CHAR_EXT_SEP = '.';
    private FileUtil() {}
    
    public static Filename getPathInfo(String url) {
        return new Filename(url, CHAR_PATH_SEP, CHAR_EXT_SEP);
    }
    public static class Filename {
        private String fullPath;
        private char pathSeparator, extensionSeparator;

        public Filename(String str, char sep, char ext) {
          fullPath = str;
          pathSeparator = sep;
          extensionSeparator = ext;
        }

        public String extension() {
          int dot = fullPath.lastIndexOf(extensionSeparator);
          return fullPath.substring(dot + 1);
        }

        public String filename() { // gets filename without extension
          int dot = fullPath.lastIndexOf(extensionSeparator);
          int sep = fullPath.lastIndexOf(pathSeparator);
          return fullPath.substring(sep + 1, dot);
        }

        public String path() {
          int sep = fullPath.lastIndexOf(pathSeparator);
          return fullPath.substring(0, sep);
        }

        public String basename() {
            int sep = fullPath.lastIndexOf(pathSeparator);
            return fullPath.substring(sep + 1);
        }
    }
}
