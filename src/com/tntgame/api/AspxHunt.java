package com.tntgame.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tnt.game.api.ConnectionException;
import tnt.game.api.Hunt;
import tnt.game.api.SessionTimeoutException;

import com.tntgame.api.fetcher.HuntFetcher;

public class AspxHunt implements Hunt {
    //private AspxUser userApi;
    private HuntFetcher fetcher;
    
    public AspxHunt(AspxUser userApi) {
        //this.userApi = userApi;
        fetcher = userApi.connection.getHunt();
    }
    
    @Override
    public State fetchCurrentState() throws ConnectionException {
        int state = fetcher.fetch();
        switch (state) {
            case HuntFetcher.MISSION_ACTIVE:
                return State.MISSION_ACTIVE;
            case HuntFetcher.IN_WORK_FINISHED:
                return State.IN_WORK_FINISHED;
            case HuntFetcher.IN_WORK:
                return State.IN_WORK;
            case HuntFetcher.IN_BOSSBATTLE:
                return State.IN_BOSSBATTLE;
            case HuntFetcher.BATTLE_REPORT:
                return State.MISSION_FINISHED;
            case HuntFetcher.FETCH_SUCCESS:
                return State.OPPONENT_CHOICE;
            case HuntFetcher.FETCH_ERROR:
                throw new ConnectionException("Document couldn't be parsed");
            default:
                throw new IllegalStateException("Invalid Fetcher return: " + state);
        }
    }
    
    @Override
    public Integer getTimer() {
        if (!fetcher.isInWork())
            return null;
        return fetcher.getSecondsLeft();
    }

    static class AspxEnemy implements Opponent {
        private int id;
        private String name;
        
        public AspxEnemy(int id, String name) {
            this.id = id;
            this.name = name;
        }
        
        @Override
        public int getId() {
            return id;
        }
        @Override
        public String getName() {
            return name;
        }
        
    }
    
    @Override
    public List<Opponent> getOpponents() {
        Map<Integer, String> enemies = fetcher.getEnemies();
        if (enemies == null)
            return null;
        List<Opponent> opponents = new ArrayList<Opponent>(3);
        for (Map.Entry<Integer, String> entry : enemies.entrySet()) {
            opponents.add(new AspxEnemy(entry.getKey(), entry.getValue()));
        }
        return opponents;
    }

    @Override
    public boolean startHunt(Opponent opponent, int duration) throws ConnectionException, SessionTimeoutException {
        fetcher.runHunt(opponent.getId(), duration);
        return true;
    }

    @Override
    public boolean abortHunt() throws SessionTimeoutException, ConnectionException {
        return fetcher.cancelHunt();
    }

    @Override
    public int getReward(Reward r) {
        String s = fetcher.getRewards();
        if (s == null)
            return 0;
        if (r == Reward.GOLD) {
            Matcher m = Pattern.compile("Gold:\\s+(\\d+)").matcher(s);
            if (m.find())
                return Integer.parseInt(m.group(1));
        } else if (r == Reward.EXP){
            Matcher m = Pattern.compile("Exp:\\s+(\\d+)").matcher(s);
            if (m.find())
                return Integer.parseInt(m.group(1));
        }
        return 0;
    }

    @Override
    public String getCurrentOpponent() {
        return fetcher.getOpponent();
    }
}
