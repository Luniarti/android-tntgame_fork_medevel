package com.tntgame.api;

import java.util.ArrayList;
import java.util.List;

import tnt.game.api.ConnectionException;
import tnt.game.api.Inventory;
import tnt.game.api.Inventory.Item.Bonus;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

import com.tntgame.api.AspxInventory.AspxItem.AspxBonus;
import com.tntgame.api.fetcher.InventoryFetcher;
import com.tntgame.api.fetcher.InventoryFetcher.ItemAttr;
import com.tntgame.api.fetcher.InventoryFetcher.ItemFetcher;

public class AspxInventory implements Inventory {
    //private AspxUser userApi;
    private InventoryFetcher fetcher;

    public AspxInventory(AspxUser aspxUser) {
        //this.userApi = aspxUser;
        this.fetcher = aspxUser.connection.getInventory();
    }
    
    static class AspxItem implements Item {
        private static final long serialVersionUID = -835821797450332002L;
        
        static class AspxBonus implements Bonus {
            private static final long serialVersionUID = -4843487132546541002L;
            
            private String label;
            
            @Override
            public String getLabel() {
                return label;
            }
        }
        
        private int id;
        private Type type;
        private Category rarity = Category.NORMAL;
        private String icon;
        private String name;
        private int attack;
        private int defense;
        private int level;
        private String restriction;
        private List<Bonus> bonus;
        
        @Override
        public int getItemId() {
            return id;
        }
        @Override
        public Type getType() {
            return type;
        }
        @Override
        public String getIcon() {
            return icon;
        }
        @Override
        public String getName() {
            return name;
        }
        @Override
        public int getAttack() {
            return attack;
        }
        @Override
        public int getDefense() {
            return defense;
        }
        @Override
        public int getLevel() {
            return level;
        }
        @Override
        public List<Bonus> getBonuses() {
            return bonus;
        }
        @Override
        public Category getCategory() {
            return rarity;
        }
        @Override
        public String getRestriction() {
            return restriction;
        }
    }

    @Override
    synchronized public List<Item> fetchInventory(Type type) throws ConnectionException {
        if (fetcher.fetch(type.value()) != InventoryFetcher.FETCH_SUCCESS)
            throw new ConnectionException("Failed to parse Inventory: " + type);
        List<ItemFetcher> itemEntries = fetcher.getItems(type.value());
        List<Item> ret = new ArrayList<Item>(itemEntries.size());
        for (ItemFetcher entry : itemEntries) {
            AspxItem item = new AspxItem();
            
            item.id = entry.id;
            item.type = type;
            item.name = InventoryFetcher.get(entry, ItemAttr.NAME);
            item.icon = InventoryFetcher.getIcon(entry).replace("\\", "/");
            item.restriction = InventoryFetcher.get(entry, ItemAttr.RESTRICTION);
            
            String color = InventoryFetcher.getNameColor(entry);
            if (color != null && color.length() > 0) {
                for (Category v : Category.values()) {
                    if (color.equals(v.getOriginalColor())) {
                        item.rarity = v;
                        break;
                    }
                }
            }
            
            try {
                String v;
                v = InventoryFetcher.get(entry, ItemAttr.ATTACK);
                item.attack = Integer.parseInt(v);
                v = InventoryFetcher.get(entry, ItemAttr.DEFENSE);
                item.defense = Integer.parseInt(v);
                v = InventoryFetcher.get(entry, ItemAttr.LEVEL);
                item.level = Integer.parseInt(v);
            } catch (NumberFormatException e) {
                Log.i("tnt.game", "Failed to parse Item-Tooltip: " + entry.id);
            }
            
            String bonus = InventoryFetcher.getBonus(entry);
            
            if (bonus != null) {
                String[] split = bonus.split("<br(?:\\s*/)?>");
                item.bonus = new ArrayList<Bonus>(split.length);
                for (int i = 0; i < split.length; i++) {
                    final String b = split[i].trim();
                    if (b.length() > 0) {
                        AspxBonus bonus2 = new AspxItem.AspxBonus();
                        bonus2.label = b;
                        item.bonus.add(bonus2);
                    }
                }
            }
            
            ret.add(item);
        }
        return ret;
    }
    
    private static ItemFetcher find(List<ItemFetcher> list, int id) {
        for (ItemFetcher item : list)
            if (item.id == id)
                return item;
        return null;
    }

    @Override
    synchronized public boolean equipItem(Item item) throws ConnectionException, SessionTimeoutException {
        if (!fetcher.isDataAvailable(item.getType().value())) {
            if (fetcher.fetch(item.getType().value()) != InventoryFetcher.FETCH_SUCCESS)
                throw new ConnectionException("Failed to parse Inventory: " + item.getType());
        }
        
        List<ItemFetcher> itemEntries = fetcher.getItems(item.getType().value());
        if (itemEntries == null)
            return false;
        ItemFetcher tt = find(itemEntries, item.getItemId());
        if (tt == null) { // ray again
            if (fetcher.fetch(item.getType().value()) != InventoryFetcher.FETCH_SUCCESS)
                throw new ConnectionException("Failed to parse Inventory: " + item.getType());
            itemEntries = fetcher.getItems(item.getType().value());
            if (itemEntries == null)
                return false;
            tt = find(itemEntries, item.getItemId());
            if (tt == null)
                return false;
        }
        return fetcher.equip(tt) == InventoryFetcher.FETCH_SUCCESS;
    }

    @Override
    public boolean unequipItem(Item item) throws ConnectionException, SessionTimeoutException {
        return equipItem(item); // the button is called the same, therefore only 1 method needed
    }

    @Override
    synchronized public boolean unequipEverything() throws ConnectionException, SessionTimeoutException {
        return fetcher.unequipEverything(Type.EQUIPMENT.value()) == InventoryFetcher.FETCH_SUCCESS;
    }

}
