package com.tntgame.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;

import tnt.game.api.ConnectionException;
import tnt.game.api.General;
import tnt.game.core.Container;
import tnt.game.storage.UserEntry;
import android.util.Log;

import com.tntgame.api.fetcher.AbstractFetcher;

public class AspxWebGame implements General {
    private Container container;
    
    private Map<Long, AspxUser> uapis = new HashMap<Long, AspxUser>();
    
    public AspxWebGame(Container container) {
        this.container = container;
    }
    
    @Override
    public boolean login(UserEntry entry, String pass) throws ConnectionException {
        entry.setHash(hash(pass));
        return login(entry);
    }

    @Override
    public boolean login(UserEntry entry) throws ConnectionException {
        AspxUser api = getUserAPI(entry);
        boolean success = api.validateLogin();
        if (!success) {
            uapis.remove(entry);
        }
        return success;
    }
    
    @Override
    public AspxUser getUserAPI(UserEntry user) {
        AspxUser api = uapis.get(user.getId());
        if (api == null) {
            api = new AspxUser(container, user);
            uapis.put(user.getId(), api);
        } else {
            api.user = user;
        }
        return api;
    }
    
    @Override
    public boolean clearUser(UserEntry user) {
        if (user == null)
            return false;
        if (uapis.containsKey(user.getId())) {
            uapis.get(user.getId()).getSession().id = null;
        }
        return uapis.remove(user.getId()) != null;
    }
    
    protected String hash(String password) {
        try {
            StringBuilder hash = new StringBuilder();
            byte[] digest = MessageDigest.getInstance("MD5").digest(password.getBytes("UTF-8"));
            for (int i = 0; i < digest.length; i++) {
                if (i > 0) {
                    hash.append("-");
                }
                hash.append(String.format("%02X", digest[i]));
            }
            return hash.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e("tnt.game", "Exception: Failed to get Password-Hash", e);
            return null;
        } catch (UnsupportedEncodingException e) {
            Log.e("tnt.game", "Exception: Failed to get Password-Hash", e);
            return null;
        }
    }

    @Override
    public InputStream loadImage(String file) throws ConnectionException {
        HttpResponse response = AbstractFetcher.fetchStatic(AbstractFetcher.URI_BASE + file);
        if (response.getStatusLine().getStatusCode() != 200) {
            Log.e("tnt.game", "Exception: Couldn't access " + AbstractFetcher.URI_BASE + file + ": " + response.getStatusLine().getReasonPhrase());
            throw new ConnectionException("Image not found");
        }
        try {
            return response.getEntity().getContent();
        } catch (IllegalStateException e) {
            Log.e("tnt.game", "Exception: Couldn't access " + AbstractFetcher.URI_BASE + file, e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Couldn't access " + AbstractFetcher.URI_BASE + file, e);
            throw new ConnectionException(e);
        }
    }
}
