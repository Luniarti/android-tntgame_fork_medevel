package com.tntgame.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpResponse;

import tnt.game.api.BossBattle;
import tnt.game.api.ConnectionException;
import tnt.game.api.Guard;
import tnt.game.api.Hunt;
import tnt.game.api.Inventory;
import tnt.game.api.Messages;
import tnt.game.api.Missions;
import tnt.game.api.SessionTimeoutException;
import tnt.game.api.User;
import tnt.game.core.Activity;
import tnt.game.core.Container;
import tnt.game.messages.MessageReceiver;
import tnt.game.storage.UserEntry;
import android.content.Intent;
import android.util.Log;

import com.tntgame.api.fetcher.AbstractFetcher;
import com.tntgame.api.fetcher.GeneralFetcher;
import com.tntgame.api.fetcher.StatusInfo;
import com.tntgame.api.fetcher.StatusInfo.ATTR;

import de.jaschastarke.utils.FileUtil;
import de.jaschastarke.utils.IOUtil;

public class AspxUser implements User {
    protected UserEntry user;
    protected Container container;
    protected GeneralFetcher connection;
    private StatusInfo fetcher;
    private AspxSession session;
    
    public static class AspxSession implements Session {
        public String id;
        public Date ts;
        @Override
        public String getSessionId() {
            return id;
        }
        @Override
        public Date getSessionTimestamp() {
            return ts;
        }
    }

    public AspxUser(Container container, UserEntry user) {
        this.container = container;
        this.user = user;
        session = new AspxSession();
        connection = new GeneralFetcher(this);
        fetcher = connection.getStatus();
    }
    
    public AspxSession getSession() {
        return session;
    }

    @Override
    public void refresh() throws ConnectionException {
        fetcher.fetch();
    }
    
    public boolean validateLogin() throws ConnectionException {
        session.id = null;
        fetcher.setInitialLogin(true);
        if (fetcher.fetch() != StatusInfo.FETCH_ERROR) {
            return fetcher.isDataAvailable();
        } else {
            throw new ConnectionException();
        }
    }
    
    public String getLoginID() {
        return "Login=" + user.getName() + "&Password=" + user.getHash();
    }
    
    public int getLevel() {
        String v = fetcher.get(StatusInfo.ATTR.LEVEL);
        if (v == null)
            return -1;
        return Integer.parseInt(v);
    }

    public String getExpLeft() {
        return fetcher.get(StatusInfo.ATTR.NEXT_LEVEL);
    }

    protected String getImagePath() {
        return fetcher.getUserImagePath();
    }
    public String getImage() {
        String i = getImagePath();
        if (i == null)
            return null;
        return FileUtil.getPathInfo(i).basename();
    }
    
    @Override
    public boolean cacheImage(File file) {
        String img = getImage();
        if (img != null && (!file.exists() || !img.equals(user.getUserImg()))) {
            Log.d("tnt.game.StatusCacheImage", !file.exists() ? "new" : (img + " != " + user.getUserImg()));
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
                FileOutputStream output = new FileOutputStream(file);
                boolean success = loadImage(output);
                output.close();
                return success;
            } catch (FileNotFoundException e) {
                Log.e("tnt.game", "Exception: Couldn't access " + file.toString(), e);
            } catch (IOException e) {
                Log.e("tnt.game", "Exception: Couldn't access " + file.toString(), e);
            } catch (ConnectionException e) {
                Log.e("tnt.game", "Exception: Failed to load Character-Image", e);
            }
        }
        return false;
    }
    
    private boolean loadImage(FileOutputStream output) throws ConnectionException {
        String img = getImagePath();
        HttpResponse r = AbstractFetcher.fetchStatic(img);
        if (r != null) {
            try {
                IOUtil.copy(r.getEntity().getContent(), output);
                user.setUserImg(FileUtil.getPathInfo(img).basename());
                return true;
            } catch (IllegalStateException e) {
                Log.e("tnt.game", "Exception: Failed to load Character-Image", e);
            } catch (IOException e) {
                Log.e("tnt.game", "Exception: Failed to load Character-Image", e);
            }
        } else {
            Log.w("tnt.game.Char-Image", "Failed to load Character-Image: File not Found");
        }
        return false;
    }
    
    public int getMoney(Money m) {
        if (fetcher == null)
            return 0;
        String v = null;
        switch (m) {
            case GOLD:
                v = fetcher.get(ATTR.GOLD);
                break;
            case RELICTS:
                v = fetcher.get(ATTR.RELICTS);
                break;
            case DIAS:
                v = fetcher.get(ATTR.DIAS);
                break;
        }
        if (v == null)
            return 0;
        return Integer.parseInt(v);
    }
    public int getStat(Stats m) {
        if (fetcher == null)
            return 0;
        String v = null;
        switch (m) {
            case MAIN:
                v = fetcher.get(ATTR.MAIN);
                break;
            case DEFENSE:
                v = fetcher.get(ATTR.DEFENSE);
                break;
            case LIFE:
                v = fetcher.get(ATTR.LIFE);
                break;
            case LUCK:
                v = fetcher.get(ATTR.LUCK);
                break;
            case UNUSED:
                v = fetcher.get(ATTR.SKILL_POINTS);
                break;
        }
        if (v == null)
            return 0;
        return Integer.parseInt(v);
    }

    public int getHealth() {
        if (fetcher == null)
            return 0;
        String v = fetcher.get(ATTR.HEALTH_POINTS);
        if (v == null)
            return 0;
        return Integer.parseInt(v);
    }
    public String getRank() {
        if (fetcher == null)
            return null;
        return fetcher.get(ATTR.RANK);
    }

    public String getEndurance() {
        if (fetcher == null)
            return null;
        return fetcher.get(ATTR.ENDURANCE);
    }

    private AspxMissions missionApi;
    @Override
    public Missions getMissionsAPI() {
        if (missionApi == null)
            missionApi = new AspxMissions(this);
        return missionApi;
    }
    private AspxTownDefense guardApi;
    @Override
    public Guard getGuardAPI() {
        if (guardApi == null)
            guardApi = new AspxTownDefense(this);
        return guardApi;
    }

    private AspxHunt huntApi;
    @Override
    public Hunt getHuntAPI() {
        if (huntApi == null)
            huntApi = new AspxHunt(this);
        return huntApi;
    }

    private AspxInBossBattle bossApi;
    @Override
    public BossBattle getBossAPI() {
        if (bossApi == null)
            bossApi = new AspxInBossBattle(this);
        return bossApi;
    }

    private AspxMessages messageApi;
    @Override
    public Messages getMessagesAPI() {
        if (messageApi == null)
            messageApi = new AspxMessages(this);
        return messageApi;
    }

    private AspxInventory inventoryApi;
    @Override
    public Inventory getInventoryAPI() {
        if (inventoryApi == null)
            inventoryApi = new AspxInventory(this);
        return inventoryApi;
    }

    @Override
    public MainStat getMaintStat() {
        String v = fetcher.get(ATTR.MAIN_STAT);
        if (v == null)
            return null;
        if (v.equalsIgnoreCase("Intelligenz:"))
            return MainStat.INTELLIGENCE;
        if (v.equalsIgnoreCase("Stärke:"))
            return MainStat.STRENGTH;
        if (v.equalsIgnoreCase("Beweglichkeit:"))
            return MainStat.AGILITY;
        if (v.equalsIgnoreCase("Angriff:"))
            return MainStat.ATTACK;
        return null;
    }

    @Override
    public String getDisplayName() {
        return fetcher.get(ATTR.NAME);
    }

    @Override
    public boolean hasNewMessages() {
        return connection.isNewMessageAvailable();
    }
    
    public void notifyAboutNotificationUpdate() {
        Intent intent = new Intent(MessageReceiver.NEW_MESSAGE_ACTION);
        intent.putExtra(Activity.EXTRA_USER_ID, user.getId());
        container.broadcast(intent);
    }
    
    public int getPrice(Stats m) {
        if (fetcher == null)
            return 0;
        String v = null;
        switch (m) {
        	case MAIN:
        		v = fetcher.get(ATTR.PMAIN);
        		break;
        	case DEFENSE:
        		v = fetcher.get(ATTR.PDEFENSE);
        		break;
        	case LIFE:
        		v = fetcher.get(ATTR.PLIFE);
        		break;
        	case LUCK:
        		v = fetcher.get(ATTR.PLUCK);
        		break;
            case UNUSED:
                break;
        }
        if (v == null)
            return 0;

        String[] split = v.split(" ");
        return Integer.parseInt(split[0]);
    }
    
    public int doStatInc(Stats m){
    	int ret = -1;
        if (fetcher == null)
            return -1;
        int type = 0;
        switch (m) {
        	case MAIN:
        		type = 1;
        		break;
        	case DEFENSE:
        		type = 2;
        		break;
        	case LIFE:
        		type = 3;
        		break;
        	case LUCK:
        		type = 4;
        		break;
            case UNUSED:
                break;
        }
        if(type != 0){
        	try {
				ret = fetcher.start(type);
			} catch (ConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -1;
			} catch (SessionTimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -1;
			}
        	return ret;
        } else {
        	return -1;
        }
        
    }
}
