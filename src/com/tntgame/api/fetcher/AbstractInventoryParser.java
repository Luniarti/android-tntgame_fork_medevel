package com.tntgame.api.fetcher;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractInventoryParser extends AbstractFetcher {
    protected static final Pattern REGEX_TOOLTIP_MATCH =
        Pattern.compile("\\$create\\(Telerik\\.Web\\.UI\\.RadToolTip, \\{[^\\{]*\"targetControlID\":\"(\\w+)\"[^\\}]*\\}, null, null, \\$get\\(\"(\\w+)\"\\)\\);");
    
    public AbstractInventoryParser(GeneralFetcher connection) {
        super(connection);
    }

    protected static Map<String, String> getTooltipMap(String rawDocument) {
        Map<String, String> ret = new HashMap<String, String>();
        Matcher matcher = REGEX_TOOLTIP_MATCH.matcher(rawDocument);
        while (matcher.find()) {
            ret.put(matcher.group(1), matcher.group(2));
        }
        return ret;
    }
}
