package com.tntgame.api.fetcher;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public abstract class AbstractParser {
    public interface Selection {
        public String getSelector();
    }
    
    protected static String get(Element node, String val) {
        if (node != null) {
            Elements els = node.select(val);
            if (els.size() < 1)
                return null;
            return getContent(els.get(0));
        }
        return null;
    }
    protected static String getHtml(Element node, String val) {
        if (node != null) {
            Elements els = node.select(val);
            if (els.size() < 1)
                return null;
            return getHTMLContent(els.get(0));
        }
        return null;
    }
    protected static String getContent(Element node) {
        if (node == null)
            return null;
        return node.text();
    }
    protected static String getHTMLContent(Element node) {
        if (node == null)
            return null;
        if (node.children().size() > 0) {
            if (node.child(0).nodeName().equalsIgnoreCase("font"))
                node = node.child(0);
        }
        return node.html();
    }
    protected static String get(Element node, Selection val) {
        return get(node, val.getSelector());
    }
}
