package com.tntgame.api.fetcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public abstract class AbstractFetcher extends AbstractParser {
    protected static final String HTTP_DOMAIN = "tnt-game.com";
    protected static final String SESSION_ID_COOKIE = "ASP.NET_SessionId";
    protected static final String USERLOGIN_COOKIE = "LoginID";
    protected static final int TIMEOUT = 60000; // ms
    private static final String REGISTER_PAGE = "Register.asp";
    private static final String ERROR_PATH = "aspxerrorpath";
    private static final String URI_LOGIN = "http://" + HTTP_DOMAIN + "/Main/WebGame/Login.aspx";
    private static final String IS_LOGGEDIN_STR = "Diamond.aspx?id="; // A unique string, that is only found, if the player is loggedin
    public static final String URI_BASE = "http://" + HTTP_DOMAIN + "/Main/WebGame/";
    public static final String USER_AGENT = "TnT-Game Android";
    
    protected GeneralFetcher connection;
    
    public AbstractFetcher(GeneralFetcher connection) {
        if (connection == null)
            throw new IllegalArgumentException("connection can not be null");
        this.connection = connection;
    }
    
    public static final int FETCH_SUCCESS = 0;
    public static final int FETCH_ERROR = 1;
    public abstract int fetch() throws ConnectionException;
    public abstract boolean isDataAvailable();
    
    protected String readString(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream is = entity.getContent();
        InputStreamReader reader = new InputStreamReader(is, "UTF-8");
        StringBuilder sb = new StringBuilder();
        char[] buffer = new char[1024];
        int n;
        while ((n = reader.read(buffer)) > -1) {
            sb.append(buffer, 0, n);
        }
        return sb.toString();
    }
    
    protected Connection newConnection(String url) {
        return Jsoup.connect(url)
                .timeout(TIMEOUT)
                .cookie(SESSION_ID_COOKIE, connection.getSessionId())
                .userAgent(USER_AGENT);
    }
    
    /**
     * Ensures Login
     * @throws AuthenticationException When the Session is timeouted and couldn't be reestablished
     */
    protected Response request(Connection con) throws IOException, AuthenticationException, ConnectionException {
        String originalUrl = con.request().url().toString(); // BUG Jsoup modifies request object on execute
        Response response = con.execute();
        if (containsError(response)) {
            Log.w("tnt.game", "Response contains Error: " + originalUrl);
            throw new ConnectionException("Asp-Error");
        }
        if (isLoginRequried(response)) {
            Log.i("tnt.game", "Response needs relogin: " + originalUrl);
            Response loginResponse = Jsoup.connect(URI_LOGIN)
                .cookie(USERLOGIN_COOKIE, connection.getLoginID())
                .followRedirects(false)
                .execute();
            if (loginResponse.statusCode() != 302)
                throw new AuthenticationException("Relogin after Session-Timeout failed");
            String session = loginResponse.cookie(SESSION_ID_COOKIE);
            connection.updateSessionId(session);
            con.cookie(SESSION_ID_COOKIE, session);
            con.url(originalUrl);
            response = con.execute();
        } else {
            connection.updateSessionId();
        }
        return response;
    }
    
    protected Document updateGlobals(Document fromDocument) {
        if (fromDocument != null)
            connection.updateGlobals(fromDocument);
        return fromDocument;
    }
    
    private static boolean isLoginRequried(Response response) {
        if (response.statusCode() == 302 && response.header("Location").contains(REGISTER_PAGE)) {
            return true;
        } else if (response.url().toString().contains(REGISTER_PAGE)) {
            return true;
        } else if (response.statusCode() == 200 && !response.body().contains(IS_LOGGEDIN_STR)) {
            return true;
        }
        return false;
    }
    private static boolean containsError(Response response) {
        if (response.statusCode() == 302 && response.header("Location").contains(ERROR_PATH)) {
            return true;
        } else if (response.url().toString().contains(ERROR_PATH)) {
            return true;
        }
        return false;
    }
    
    protected static Response validate(Response response) throws SessionTimeoutException, ConnectionException {
        if (containsError(response))
            throw new ConnectionException("Asp-Error");
        if (isLoginRequried(response))
            throw new SessionTimeoutException("Session-Timeouted");
        return response;
    }
    
    public static HttpResponse fetchStatic(String url) throws ConnectionException {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            request.setHeader("User-Agent", USER_AGENT);
            
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, TIMEOUT);
            
            request.setParams(params);
            
            HttpResponse response = client.execute(request);
            
            if (response.getStatusLine().getStatusCode() == 200) {
                return response;
            } else {
                Log.i("tnt.game.fetchStatic", url + " - " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            }
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetchStatic " + url, e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetchStatic " + url, e);
            throw new ConnectionException(e);
        }
        return null;
    }
    
    public Connection prepareForm(Element form) {
        Element formNode = form;
        if (!formNode.nodeName().equalsIgnoreCase("form"))
            formNode = form.select("form").first();
        if (formNode == null)
            formNode = form.parents().select("form").first();
        Connection c = newConnection(formNode.absUrl("action"));
        for (Element el : formNode.select("input, textarea, select")) {
            if (el.hasAttr("name")) {
                if (el.nodeName().equalsIgnoreCase("input")) {
                    String t = el.attr("type");
                    if (t.equalsIgnoreCase("checkbox") || t.equalsIgnoreCase("radio")) {
                        if (el.hasAttr("selected") && el.hasAttr("name"))
                            c.data(el.attr("name"), el.val());
                    } else if (t.equalsIgnoreCase("button") || t.equalsIgnoreCase("submit") || t.equalsIgnoreCase("image")) {
                        // don't add
                    } else if (el.hasAttr("name")) {
                        c.data(el.attr("name"), el.val());
                    }
                } else {
                    c.data(el.attr("name"), el.val());
                }
            }
        }
        if (formNode.hasAttr("method"))
            c.method(Connection.Method.valueOf(formNode.attr("method").toUpperCase(Locale.US)));
        return c;
    }
}
