package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class MessageFetcher extends AbstractFetcher {
    private static final String URI_MESSAGES = "http://" + HTTP_DOMAIN + "/Main/WebGame/ReadMessage.aspx";
    private static final String URI_NEW_MESSAGE = "http://" + HTTP_DOMAIN + "/Main/WebGame/WriteMessage.aspx";
    private static final String URI_MESSAGE = "http://" + HTTP_DOMAIN + "/Main/WebGame/ShowMessage.aspx";
    private static final Pattern MID_REGEX = Pattern.compile("mID=(\\d+)$");
    
    public MessageFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    private Document inbox;
    private Document outbox;
    private Document selectedMessage;
    private Document writeMessage;
    
    public int fetch(int id) throws ConnectionException {
        try {
            Connection con = newConnection(URI_MESSAGES)
                    .followRedirects(true);
            
            if (id != 0)
                con.data("id", String.valueOf(id));
            
            Response response = request(con);
            
            Document document = updateGlobals(response.parse());
            
            if (id == 0)
                inbox = document;
            else
                outbox = document;
            
            return document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public int deleteAll(int id) throws SessionTimeoutException, ConnectionException {
        Document document;
        if (id == 0)
            document = inbox;
        else
            document = outbox;
        
        if (document == null)
            return FETCH_ERROR;
        
        try {
            validate(prepareForm(document.select("form").first())
                .data("ctl00$ContentPlaceHolder1$ImageButton2.x", "1")
                .data("ctl00$ContentPlaceHolder1$ImageButton2.y", "1")
                .execute());
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        
        return FETCH_SUCCESS;
    }

    @Override
    public int fetch() throws ConnectionException {
        return fetch(0);
    }

    @Override
    public boolean isDataAvailable() {
        return inbox != null;
    }
    
    public static class MessageData {
        public int id;
        public boolean bold;
        public String subject;
    }
    
    public List<MessageData> getMessages(int type) {
        Document document = type == 1 ? outbox : inbox;
        
        Elements rows = document.select("#ctl00_ContentPlaceHolder1_tblMain tr");
        List<MessageData> list = new ArrayList<MessageData>(rows.size());
        for (Element row : rows) {
            Element a = row.select("a").first();
            if (a != null) {
                Matcher m = MID_REGEX.matcher(a.attr("href"));
                if (m.find()) {
                    MessageData d = new MessageData();
                    d.id = Integer.parseInt(m.group(1));
                    Element b = a.select("b").first();
                    if (b != null) {
                        d.bold = true;
                        d.subject = b.text();
                    } else {
                        d.bold = false;
                        d.subject = a.text();
                    }
                    list.add(d);
                }
            }
        }
        
        return list;
    }

    public int fetchMessage(int id) throws ConnectionException {
        try {
            Connection con = newConnection(URI_MESSAGE)
                    .followRedirects(true);
            
            con.data("mID", String.valueOf(id));
            
            Response response = request(con);
            
            selectedMessage = updateGlobals(response.parse());
            
            return selectedMessage != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    public String get(MsgAttr attr) {
        if (selectedMessage == null)
            return null;
        return get(selectedMessage, attr);
    }
    public String getMsgContent() {
        if (selectedMessage == null)
            return null;
        return getHtml(selectedMessage, "#ctl00_ContentPlaceHolder1_lbMessage");
    }
    
    public enum MsgAttr implements Selection {
        FROM("#ctl00_ContentPlaceHolder1_lbFrom"),
        SUBJECT("#ctl00_ContentPlaceHolder1_lbSubjekt"),
        DATE("#ctl00_ContentPlaceHolder1_lbTime");

        private String sel;
        MsgAttr(String selector) {
            this.sel = selector;
        }
        @Override
        public String getSelector() {
            return sel;
        }
        
    }

    public int send(String recipient, String subject, String content) throws ConnectionException {
        try {
            if (writeMessage == null) {
                writeMessage = request(newConnection(URI_NEW_MESSAGE).followRedirects(false)).parse();
            }
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbName").first().val(recipient);
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbSubjekt").first().val(subject);
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbmessage").first().val(content);
            
            Response response = request(prepareForm(writeMessage.select("form").first())
                    .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                    .data("ctl00$ContentPlaceHolder1$Button1.y", "1"));
            
            return response.body().contains("Nachricht gesendet") ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public int deleteMessage(int id) throws ConnectionException {
        if (selectedMessage == null) {
            int r = fetchMessage(id);
            if (r != FETCH_SUCCESS)
                return r;
        }
        Element form = selectedMessage.select("form").first();
        Matcher m = MID_REGEX.matcher(form.attr("action"));
        if (m.find()) {
            int mID = Integer.parseInt(m.group(1));
            if (mID != id) {
                int r = fetchMessage(id);
                if (r != FETCH_SUCCESS)
                    return r;
                form = selectedMessage.select("form").first();
            }
            
            try {
                validate(prepareForm(form)
                    .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                    .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                    .execute());
                return FETCH_SUCCESS;
            } catch (SessionTimeoutException e) {
                Log.e("tnt.game", "Exception: Failed to fetch Website", e);
                throw new ConnectionException(e);
            } catch (IOException e) {
                Log.e("tnt.game", "Exception: Failed to fetch Website", e);
                throw new ConnectionException(e);
            }
        }
        return FETCH_ERROR;
    }
}
