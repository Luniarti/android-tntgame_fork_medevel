package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class InventoryFetcher extends AbstractInventoryParser {
    private static final String URI_INVENTORY = "http://" + HTTP_DOMAIN + "/Main/WebGame/Inventory.aspx";
    private static final String URI_EQUIPMENT = "http://" + HTTP_DOMAIN + "/Main/WebGame/EquipedItems.aspx";
    private static final String URI_TREASURE = "http://" + HTTP_DOMAIN + "/Main/WebGame/Treasure.aspx";
    private static final Pattern REGEX_STYLE_COLOR = Pattern.compile("color:\\s*([#\\w]+)");
    
    public InventoryFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    private static class InventoryDocument {
        private Document document;
        private Map<String, String> tooltips;
    }
    
    private InventoryDocument[] documents = new InventoryDocument[3];

    @Override
    public int fetch() throws ConnectionException {
        return fetch(0);
    }
    @Override
    public boolean isDataAvailable() {
        return isDataAvailable(0);
    }

    public int fetch(int type) throws ConnectionException {
        String url;
        switch (type) {
            case 0:
                url = URI_INVENTORY;
                break;
            case 1:
                url = URI_EQUIPMENT;
                break;
            case 2:
                url = URI_TREASURE;
                break;
            default:
                return FETCH_ERROR;
        }

        try {
            Connection con = newConnection(url)
                    .followRedirects(false);
            
            Response response = request(con);
            
            InventoryDocument d = new InventoryDocument();
            d.tooltips = getTooltipMap(response.body());
            d.document = updateGlobals(response.parse());
            documents[type] = d;
            
            Log.d("tnt.game", "Found Tooltips in Inventory: " + d.tooltips.size());
            
            return d.document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    public boolean isDataAvailable(int type) {
        return documents[type] != null;
    }
    
    public static class ItemFetcher {
        public int id;
        public Element tooltip;
    }
    
    public List<ItemFetcher> getItems(int type) {
        if (documents[type] == null)
            return null;
        Document document = documents[type].document;
        Elements items = document.select("#ctl00_ContentPlaceHolder1_tblMain input[type=image]");
        List<ItemFetcher> ret = new ArrayList<ItemFetcher>();
        for (Element item : items) {
            ItemFetcher f = new ItemFetcher();
            try {
                if (item.attr("itemid") == null || item.attr("itemid").length() == 0)
                    continue;
                f.id = Integer.parseInt(item.attr("itemid"));
            } catch (NumberFormatException e) {
                continue;
            }
            String tooltipId = documents[type].tooltips.get(item.attr("id"));
            
            f.tooltip = document.select("#" + tooltipId + " table table").first();
            if (f.tooltip == null)
                Log.w("tnt.game", "Didn't find tooltip for item: " + item.attr("id"));
            ret.add(f);
        }
        return ret;
    }
    
    public enum ItemAttr implements Selection {
        NAME("tr:eq(0) > td:eq(1) > span"),
        ATTACK("tr:eq(1) > td:eq(1) > span"),
        DEFENSE("tr:eq(2) > td:eq(1) > span"),
        LEVEL("tr:eq(3) > td:eq(1) > span"),
        RESTRICTION("tr:eq(4) > td:eq(1) > span");
        //RESTRICTION("[spanid$=1bError]");
        private String sel;
        ItemAttr(String selector) {
            sel = selector;
        }
        @Override
        public String getSelector() {
            return sel;
        }
    }
    
    public static String get(ItemFetcher item, ItemAttr attr) {
        return get(item.tooltip, (Selection) attr);
    }
    public static String getIcon(ItemFetcher item) {
        Element n = item.tooltip.select("tr:eq(0) img").first();
        return n != null ? n.attr("src") : null;
    }
    public static String getBonus(ItemFetcher item) {
        return getHTMLContent(item.tooltip.select("tr:eq(4) > td > span").last());
    }
    public static String getNameColor(ItemFetcher item) {
        Element e = item.tooltip.select(ItemAttr.NAME.getSelector()).first();
        Element span = e.children().select("span").first();
        while (span != null && span.children().select("span").size() > 0)
            span = span.children().select("span").first();
        if (span != null) {
            String style = span.attr("style");
            if (style != null && style.length() > 0) {
                Matcher m = REGEX_STYLE_COLOR.matcher(style);
                if (m.find()) {
                    return m.group(1);
                }
            }
        }
        return null;
    }
    public int equip(ItemFetcher item) throws SessionTimeoutException, ConnectionException {
        String btnName = item.tooltip.select("input[type=image][text=Anlegen]").attr("name");
        try {
            validate(prepareForm(item.tooltip)
                    .data(btnName + ".x", "1")
                    .data(btnName + ".y", "1")
                    .followRedirects(false)
                    .execute());
            return FETCH_SUCCESS;
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    public int unequipEverything(int value) throws SessionTimeoutException, ConnectionException {
        Document document = documents[value].document;
        String btnName = document.select("input[type=image][title=Alles ablegen]").attr("name");
        try {
            validate(prepareForm(document.select("form").first())
                    .data(btnName + ".x", "1")
                    .data(btnName + ".y", "1")
                    .followRedirects(false)
                    .execute());
            return FETCH_SUCCESS;
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
}
