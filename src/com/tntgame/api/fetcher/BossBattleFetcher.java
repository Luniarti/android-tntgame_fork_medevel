package com.tntgame.api.fetcher;

import org.jsoup.nodes.Document;

import tnt.game.api.ConnectionException;

public class BossBattleFetcher extends AbstractFetcher {
    protected Document document;
    
    public BossBattleFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    public int getSecondsLeft() {
        if (document == null)
            return -1;
        String t = document.select("#c1").text();
        if (t == null || t.length() == 0) // sometimes it happens, that there is no time left...
            return 0;
        return Integer.parseInt(t);
    }

    @Override
    public int fetch() throws ConnectionException {
        throw new IllegalAccessError("Manually call not needed");
    }

    @Override
    public boolean isDataAvailable() {
        return document != null;
    }
}
