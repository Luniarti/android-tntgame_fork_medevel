package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class MissionsFetcher extends AbstractFetcher {
    private static final String URI_MISSIONS = "http://" + HTTP_DOMAIN + "/Main/WebGame/Missions.aspx";
    protected Document document;
    private Document selectedMission;
    protected Document lastBattle = null;
    protected boolean in_work = false;
    
    public MissionsFetcher(GeneralFetcher connection) {
        super(connection);
    }

    public static final int HUNT_FINISHED = -4;
    public static final int IN_BOSSBATTLE = -3;
    public static final int BATTLE_REPORT = -2;
    public static final int HUNT_ACTIVE = -1;
    public static final int IN_WORK = 2;
    public int fetch() throws ConnectionException {
        document = null;
        lastBattle = null;
        in_work = false;
        try {
            Response response = request(newConnection(URI_MISSIONS)
                    .followRedirects(true));
            
            if (response.url().toString().contains("InWork.aspx")) {
                if (response.body().contains("Beute von der Jagd")) {
                    connection.getHunt().document = updateGlobals(response.parse());
                    connection.getHunt().in_work = false;
                    connection.getHunt().in_work_finished = true;
                    return HUNT_FINISHED;
                } else if (response.body().contains("Du bist derzeit auf der Jagd")) {
                    connection.getHunt().document = response.parse();
                    connection.getHunt().in_work_finished = false;
                    connection.getHunt().in_work = true;
                    return HUNT_ACTIVE;
                }
                document = updateGlobals(response.parse());
                in_work = true;
                return document != null ? IN_WORK : FETCH_ERROR;
            } else if (response.url().toString().contains("BattleReport.aspx")) {
                lastBattle = updateGlobals(response.parse());
                return BATTLE_REPORT;
            } else if (response.url().toString().contains("InBossBattle.aspx")) {
                connection.getBossBattle().document = updateGlobals(response.parse());
                return IN_BOSSBATTLE;
            } else {
                document = updateGlobals(response.parse());
            }
            return document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public boolean isDataAvailable() {
        return document != null;
    }
    
    public boolean isInWork() {
        return in_work && isDataAvailable();
    }
    
    public int getSecondsLeft() {
        if (!isInWork())
            return -1;
        String t = document.select("#c1").text();
        if (t == null || t.length() == 0) // sometimes it happens, that there is no time left...
            return 0;
        return Integer.parseInt(t);
    }
    
    public String[] getSelectEntry(int idx) {
        Elements table = document.select("table#ctl00_ContentPlaceHolder1_tblMain tr:gt(0)");
        if (table.size() <= idx)
            return null;
        Element row = table.get(idx);
        Elements cols = row.select("td");
        return new String[]{cols.get(0).text(), cols.get(1).text()};
    }
    
    public enum SelectedMissionAttr implements Selection {
        TITLE("#ctl00_ContentPlaceHolder1_lbName"),
        DETAIL("#ctl00_ContentPlaceHolder1_lbDetails"),
        DURATION("#ctl00_ContentPlaceHolder1_lbDuration"),
        ENDURANCE("#ctl00_ContentPlaceHolder1_lbEndu"),
        REWARD("#ctl00_ContentPlaceHolder1_lblReport");
        
        private String sel;
        SelectedMissionAttr(String sel) {
            this.sel = sel;
        }
        public String getSelector() {
            return sel;
        }
    }

    public String getSelectedMissionAttr(SelectedMissionAttr attr) {
        return get(selectedMission, attr);
    }

    public boolean fetchSelectMission(int position) throws ConnectionException {
        Elements submits = document.select("#ctl00_ContentPlaceHolder1_tblMain input[type=image]");
        String sn = submits.get(position).attr("name");
        try {
            selectedMission = prepareForm(document.select("form").first())
                .data(sn + ".x", "1")
                .data(sn + ".y", "1")
                .post();
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        return selectedMission != null;
    }

    public void runSelectedMission() throws SessionTimeoutException, ConnectionException {
        try {
            Response response = validate(prepareForm(selectedMission.select("form").first())
                .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                .execute());
            
            in_work = response.url().toString().contains("InWork.aspx");
            if (in_work)
                document = response.parse();
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public BattleReportParser getBattle() {
        if (lastBattle == null)
            return null;
        return new BattleReportParser(lastBattle);
    }
}
