package com.tntgame.api.fetcher;

import org.jsoup.nodes.Document;

public class BattleReportParser extends AbstractParser {
    private Document document;
    public BattleReportParser(Document doc) {
        this.document = doc;
    }
    public enum Attr implements Selection {
        ATTACKER("#ctl00_ContentPlaceHolder1_lbAttacker"),
        DEFENDER("#ctl00_ContentPlaceHolder1_lbDefender"),
        WINNER("#ctl00_ContentPlaceHolder1_lbWinner"),
        GOLD("#ctl00_ContentPlaceHolder1_lbGold"),
        EXP("#ctl00_ContentPlaceHolder1_lbExp");
        private String sel;
        Attr(String sel) {
            this.sel = sel;
        }
        @Override
        public String getSelector() {
            return sel;
        }
    }
    public String get(Attr attr) {
        return get(document, attr);
    }
    public String[] getLogMsgs() {
        String log = getHtml(document, "#ctl00_ContentPlaceHolder1_lbBattle");
        String[] split = log.split("<br(?:\\s*/)?>");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].trim();
        }
        return split;
    }
    public String getOpponentImage() {
        return document.select("#ctl00_ContentPlaceHolder2_ctl00_Image1").first().absUrl("src");
    }
}
