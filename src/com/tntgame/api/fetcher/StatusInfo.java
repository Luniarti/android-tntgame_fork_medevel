package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class StatusInfo extends AbstractFetcher {
    private static final String URI_STATUS_INFO = "http://" + HTTP_DOMAIN + "/Main/WebGame/StatusInfo.aspx";
    private static final int FETCH_LOGIN_INVALID = -1;
    
    public static enum ATTR implements Selection {
        NAME("#ctl00_ContentPlaceHolder1_lbName"),
        LEVEL("#ctl00_ContentPlaceHolder1_lbLevel"),
        NEXT_LEVEL("#ctl00_ContentPlaceHolder1_lbExpLeft"),
        
        GOLD("#ctl00_ContentPlaceHolder6_MailCheck1_lbGold"),
        RELICTS("#ctl00_ContentPlaceHolder6_MailCheck1_lbDia"),
        DIAS("#ctl00_ContentPlaceHolder6_MailCheck1_lbDiaPrem"),

        ENDURANCE("#ctl00_ContentPlaceHolder1_lbDuration"),
        HEALTH_POINTS("#ctl00_ContentPlaceHolder1_lbHP"),
        RANK("#ctl00_ContentPlaceHolder1_lbRanking"),

        MAIN_STAT("#ctl00_ContentPlaceHolder1_lbAtb"),
        MAIN("#ctl00_ContentPlaceHolder1_lbStr"),
        DEFENSE("#ctl00_ContentPlaceHolder1_lbAgi"),
        LIFE("#ctl00_ContentPlaceHolder1_lbInt"),
        LUCK("#ctl00_ContentPlaceHolder1_lbLuck"),
        SKILL_POINTS("#ctl00_ContentPlaceHolder1_lbPoints"),

        PMAIN("#ctl00_ContentPlaceHolder1_lbStrPrice"),
        PDEFENSE("#ctl00_ContentPlaceHolder1_lbAgiPrice"),
        PLIFE("#ctl00_ContentPlaceHolder1_lbIntPrice"),
        PLUCK("#ctl00_ContentPlaceHolder1_lbLuckPrice");
        
        private String s;
        ATTR(String selector) {
            this.s = selector;
        }
        public String getSelector() {
            return s;
        }
    }
    
    private boolean isLogin;
    private Document document;

    public StatusInfo(GeneralFetcher connection) {
        super(connection);
    }
    
    public void setInitialLogin(boolean allowRetry) {
        this.isLogin = allowRetry;
    }
    
    public int fetch() throws ConnectionException {
        try {
            Response response;
            if (isLogin) {
                document = null;
                Connection con = Jsoup.connect(URI_STATUS_INFO)
                        .timeout(TIMEOUT)
                        .userAgent(USER_AGENT)
                        .cookie(USERLOGIN_COOKIE, connection.getLoginID())
                        .followRedirects(false);
                response = con.execute();
                if (response.statusCode() != 200) {
                    con.request().cookies().putAll(response.cookies());
                    response = con.execute();
                    if (con.response().statusCode() == 200)
                        document = response.parse();
                } else {
                    document = response.parse();
                }
                if (document != null) {
                    connection.updateSessionId(con.request().cookie(SESSION_ID_COOKIE));
                }
            } else {
                response = request(newConnection(URI_STATUS_INFO)
                    .followRedirects(false));
                document = response.parse();
            }
            
            isLogin = false;
            if (document == null)
                Log.w("tnt.game.StatusInfo.fetch", URI_STATUS_INFO + " - " + response.statusCode() + " " + response.statusMessage());
            
            updateGlobals(document);
            return document != null ? FETCH_SUCCESS : FETCH_LOGIN_INVALID;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    
    public boolean isDataAvailable() {
        return document != null;
    }
    
    public String get(ATTR attr) {
        return get(document, attr);
    }

    public String getUserImagePath() {
        if (document != null) {
            return document.select("#ctl00_ContentPlaceHolder2_Hero1_imgBase").get(0).absUrl("src");
            //return src.replaceAll("^\\.\\./", URI_MAIN_PATH);
        }
        return null;
    }
    
    public int start(int attr) throws ConnectionException, SessionTimeoutException {
        
    	String x,y;
    	switch(attr){
    		case 1:
    			x = "ctl00$ContentPlaceHolder1$Button1.x";
    			y = "ctl00$ContentPlaceHolder1$Button1.y";
    			break;
    		case 2:
    			x = "ctl00$ContentPlaceHolder1$Button2.x";
    			y = "ctl00$ContentPlaceHolder1$Button2.y";
    			break;
    		case 3:
    			x = "ctl00$ContentPlaceHolder1$Button3.x";
    			y = "ctl00$ContentPlaceHolder1$Button3.y";
    			break;
    		case 4:
    			x = "ctl00$ContentPlaceHolder1$Button4.x";
    			y = "ctl00$ContentPlaceHolder1$Button4.y";
    			break;
    		default:
    			return -1;
    	}
        
        try {
            Response response = validate(prepareForm(document.select("form").first())
                .data(x, "1")
                .data(y, "1")
                .execute());
            if (response.body().contains("Nicht genug Gold !")){
            	return 0;
            }
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        return 1;
    }
}
