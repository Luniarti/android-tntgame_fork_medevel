package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.Guard.Difficulty;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class TownDefenseFetcher extends AbstractFetcher {
    private static final String URI_TOWNDEFENSE = "http://" + HTTP_DOMAIN + "/Main/WebGame/TownDefense.aspx";
    private Document document;
    
    public TownDefenseFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    @Override
    public int fetch() throws ConnectionException {
        document = null;
        try {
            Response response = request(newConnection(URI_TOWNDEFENSE)
                    .followRedirects(false));
            
            document = response.parse();
            updateGlobals(document);
            return document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    
    @Override
    public boolean isDataAvailable() {
        return document != null;
    }
    
    public String getCount() {
        return get(document, "#ctl00_ContentPlaceHolder1_lbBattles");
    }
    
    public int getSecondsLeft() {
        String t = document.select("#c1").text();
        if (t == null || t.length() == 0) // sometimes it happens, that there is no time left...
            return 0;
        return Integer.parseInt(t);
    }
    
    public void startNoReport(Difficulty difficulty) throws SessionTimeoutException, ConnectionException {
        String d = String.valueOf(difficulty.value());
        if (d.equals("3"))
            d = "Schwer";
        try {
        	document.select("#ctl00_ContentPlaceHolder1_ddDifficulty").first().val(d);
            validate(prepareForm(document.select("form").first())
                .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                .followRedirects(false)
                .execute());
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public BattleReportParser start(Difficulty difficulty) throws SessionTimeoutException, ConnectionException {
        String d = String.valueOf(difficulty.value());
        if (d.equals("3"))
            d = "Schwer";
        try {
            document.select("#ctl00_ContentPlaceHolder1_ddDifficulty").first().val(d);
            Response response = validate(prepareForm(document.select("form").first())
                .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                .followRedirects(true)
                .execute());
            
            if (response.body().contains("Keine Angriffe übrig"))
                throw new ConnectionException("No more Attacks available");
            
            return new BattleReportParser(updateGlobals(response.parse()));
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
}
