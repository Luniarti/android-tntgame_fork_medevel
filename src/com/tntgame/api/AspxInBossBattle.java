package com.tntgame.api;

import com.tntgame.api.fetcher.BossBattleFetcher;

import tnt.game.api.BossBattle;

public class AspxInBossBattle implements BossBattle {
    private BossBattleFetcher parse;

    public AspxInBossBattle(AspxUser aspxUser) {
        this.parse = aspxUser.connection.getBossBattle();
    }
    
    @Override
    public Integer getBossTimer() {
        if (!parse.isDataAvailable())
            return null;
        return parse.getSecondsLeft();
    }
}
