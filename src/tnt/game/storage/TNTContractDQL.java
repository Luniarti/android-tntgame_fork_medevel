package tnt.game.storage;

import tnt.game.storage.TNTContract.ActivityTable;
import tnt.game.storage.TNTContract.ItemTable;
import tnt.game.storage.TNTContract.MessageTable;
import tnt.game.storage.TNTContract.UserTable;
import android.database.sqlite.SQLiteDatabase;

public class TNTContractDQL {
    private static final String DELETE_TABLE_WITH_NAME = "DROP TABLE IF EXISTS ";
    private static final String CREATE_USER_TABLE =
            "CREATE TABLE " + UserTable.TABLE_NAME + " (" +
                UserTable._ID + " INTEGER PRIMARY KEY," +
                UserTable.COLUMN_NAME_USERNAME + " TEXT," +
                UserTable.COLUMN_NAME_HASH + " TEXT," +
                UserTable.COLUMN_NAME_USER_IMG + " TEXT," +
                UserTable.COLUMN_NAME_GUARD_DIFFICULTY + " INTEGER"+
            ")";
    private static final String CREATE_ACTIVITY_TABLE =
            "CREATE TABLE " + ActivityTable.TABLE_NAME + " (" +
                ActivityTable._ID + " INTEGER PRIMARY KEY," +
                ActivityTable.COLUMN_NAME_USER_ID + " INTEGER," +
                ActivityTable.COLUMN_NAME_TYPE + " INTEGER," +
                ActivityTable.COLUMN_NAME_TITLE + " TEXT," +
                ActivityTable.COLUMN_NAME_DURATION + " INTEGER," +
                ActivityTable.COLUMN_NAME_TIME_END + " INTEGER," +
                ActivityTable.COLUMN_NAME_NOTIFIED + " INTEGER" +
            ")";
    private static final String CREATE_MESSAGE_TABLE =
            "CREATE TABLE " + MessageTable.TABLE_NAME + " (" +
                MessageTable._ID + " INTEGER PRIMARY KEY," +
                MessageTable.COLUMN_NAME_USER_ID + " INTEGER," +
                MessageTable.COLUMN_NAME_MESSAGE_ID + " INTEGER," +
                MessageTable.COLUMN_NAME_TYPE + " INTEGER," +
                MessageTable.COLUMN_NAME_SUBJECT + " TEXT," +
                MessageTable.COLUMN_NAME_CONTACT + " TEXT," +
                MessageTable.COLUMN_NAME_IS_UNREAD + " INTEGER," +
                MessageTable.COLUMN_NAME_DATE + " INTEGER," +
                MessageTable.COLUMN_NAME_CONTENT + " TEXT" +
            ")";
    private static final String CREATE_ITEM_TABLE =
            "CREATE TABLE " + ItemTable.TABLE_NAME + " (" +
                ItemTable._ID + " INTEGER PRIMARY KEY," +
                ItemTable.COLUMN_NAME_USER_ID + " INTEGER," +
                ItemTable.COLUMN_NAME_TYPE + " INTEGER," +
                ItemTable.COLUMN_NAME_CATEGORY + " INTEGER," +
                ItemTable.COLUMN_NAME_NAME + " TEXT," +
                ItemTable.COLUMN_NAME_ICON + " TEXT," +
                ItemTable.COLUMN_NAME_ATTACK + " INTEGER," +
                ItemTable.COLUMN_NAME_DEFENSE + " INTEGER," +
                ItemTable.COLUMN_NAME_LEVEL + " INTEGER," +
                ItemTable.COLUMN_NAME_RESTRICTION + " TEXT," +
                ItemTable.COLUMN_NAME_BONUSES + " TEXT," +
                ItemTable.COLUMN_NAME_SORTING + " INTEGER" +
            ")";
    
    public static void createTables(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_ACTIVITY_TABLE);
        db.execSQL(CREATE_MESSAGE_TABLE);
        db.execSQL(CREATE_ITEM_TABLE);
    }
    public static void updateTables(SQLiteDatabase db, int fromVersion, int toVersion) {
        boolean fallback = true;
        if (fromVersion < 4) {
            db.execSQL(CREATE_ACTIVITY_TABLE);
            db.execSQL("INSERT INTO " + ActivityTable.TABLE_NAME + " (" +
                    ActivityTable.COLUMN_NAME_USER_ID + ", " +
                    ActivityTable.COLUMN_NAME_TYPE + ", " +
                    ActivityTable.COLUMN_NAME_TITLE + ", " +
                    ActivityTable.COLUMN_NAME_DURATION + ", " +
                    ActivityTable.COLUMN_NAME_TIME_END + ", " +
                    ActivityTable.COLUMN_NAME_NOTIFIED + ") " +
                " SELECT " +
                    ActivityTable.COLUMN_NAME_USER_ID + ", " +
                    "0, " +
                    ActivityTable.COLUMN_NAME_TITLE + ", " +
                    ActivityTable.COLUMN_NAME_DURATION + ", " +
                    ActivityTable.COLUMN_NAME_TIME_END + ", " +
                    ActivityTable.COLUMN_NAME_NOTIFIED +
                " FROM mission_entry");
            db.execSQL("DROP TABLE mission_entry");
            fallback = false;
        }
        if (fromVersion < 5) {
            db.execSQL(CREATE_MESSAGE_TABLE);
            fallback = false;
        }
        if (fromVersion < 7) {
            db.execSQL(DELETE_TABLE_WITH_NAME + MessageTable.TABLE_NAME);
            db.execSQL(CREATE_MESSAGE_TABLE);
            fallback = false;
        }
        if (fromVersion < 8) {
            db.execSQL(CREATE_ITEM_TABLE);
            fallback = false;
        }
        if (fromVersion < 12) {
            db.execSQL(DELETE_TABLE_WITH_NAME + ItemTable.TABLE_NAME);
            db.execSQL(CREATE_ITEM_TABLE);
            fallback = false;
        }
        if (fallback)
            recreateTables(db); // In unhandled Case, start over clean.
    }
    public static void recreateTables(SQLiteDatabase db) {
        db.execSQL(DELETE_TABLE_WITH_NAME + UserTable.TABLE_NAME);
        db.execSQL(DELETE_TABLE_WITH_NAME + ActivityTable.TABLE_NAME);
        db.execSQL(DELETE_TABLE_WITH_NAME + MessageTable.TABLE_NAME);
        db.execSQL(DELETE_TABLE_WITH_NAME + ItemTable.TABLE_NAME);
        createTables(db);
    }
    
}
