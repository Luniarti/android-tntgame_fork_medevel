package tnt.game.storage;

import java.util.Date;

import tnt.game.api.Messages.BoxType;
import tnt.game.api.Messages.Message;
import tnt.game.api.Messages.MessageHeader;
import tnt.game.storage.TNTContract.MessageTable;
import android.database.Cursor;
import de.jaschastarke.android.db.AbstractStringEntry;
import de.jaschastarke.android.db.DBTable;

public class MessageEntry extends AbstractStringEntry implements Message {
    private static final long serialVersionUID = -4810992459435431238L;

    public MessageEntry(Cursor cursor) {
        super(cursor);
    }
    
    public MessageEntry(UserEntry user, MessageHeader header, BoxType type) {
        set(MessageTable.COLUMN_NAME_MESSAGE_ID, String.valueOf(header.getMessageId()));
        set(MessageTable.COLUMN_NAME_USER_ID, String.valueOf(user.getId()));
        set(MessageTable.COLUMN_NAME_TYPE, String.valueOf(type.ordinal()));
        set(MessageTable.COLUMN_NAME_SUBJECT, header.getSubject());
        if (header.getDate() != null)
            set(MessageTable.COLUMN_NAME_DATE, String.valueOf(header.getDate().getTime()));
        setBool(MessageTable.COLUMN_NAME_IS_UNREAD, header.isUnread());
    }

    public void updateFullMessage(Message message) {
        if (message.getDate() != null)
            set(MessageTable.COLUMN_NAME_DATE, String.valueOf(message.getDate().getTime()));
        else
            set(MessageTable.COLUMN_NAME_DATE, null);
        set(MessageTable.COLUMN_NAME_CONTACT, message.getContact());
        set(MessageTable.COLUMN_NAME_CONTENT, message.getContent());
    }

    @Override
    public DBTable<?> getTable() {
        return TNTContract.MESSAGE;
    }

    public void setUnread(boolean unread) {
        setBool(MessageTable.COLUMN_NAME_IS_UNREAD, unread);
    }
    public boolean isUnread() {
        return getBool(MessageTable.COLUMN_NAME_IS_UNREAD);
    }

    public Date getDate() {
        long ts = getLong(MessageTable.COLUMN_NAME_DATE);
        if (ts == 0)
            return null;
        return new Date(ts);
    }

    public String getSubject() {
        return get(MessageTable.COLUMN_NAME_SUBJECT);
    }

    public String getContact() {
        return get(MessageTable.COLUMN_NAME_CONTACT);
    }

    public String getContent() {
        return get(MessageTable.COLUMN_NAME_CONTENT);
    }

    @Override
    public int getMessageId() {
        return getInt(MessageTable.COLUMN_NAME_MESSAGE_ID);
    }

    public BoxType getType() {
        for (BoxType v : BoxType.values()) {
            if (v.ordinal() == getInt(MessageTable.COLUMN_NAME_TYPE))
                return v;
        }
        throw new IllegalStateException("Unknown Message-Type");
    }

}
