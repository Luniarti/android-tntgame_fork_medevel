package tnt.game.storage;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import tnt.game.api.Inventory.Item;
import tnt.game.api.Inventory.Category;
import tnt.game.api.Inventory.Type;
import tnt.game.storage.TNTContract.ItemTable;
import android.database.Cursor;
import android.util.Log;
import de.jaschastarke.android.db.AbstractStringEntry;
import de.jaschastarke.android.db.DBTable;

public class ItemEntry extends AbstractStringEntry implements Item {
    private static final long serialVersionUID = 3212795377324391499L;

    public ItemEntry(UserEntry user, Item item) {
        setId(item.getItemId());
        set(ItemTable.COLUMN_NAME_USER_ID, String.valueOf(user.getId()));
        set(ItemTable.COLUMN_NAME_TYPE, String.valueOf(item.getType().value()));
        set(ItemTable.COLUMN_NAME_CATEGORY, String.valueOf(item.getCategory().ordinal()));
        set(ItemTable.COLUMN_NAME_ICON, item.getIcon());
        set(ItemTable.COLUMN_NAME_NAME, item.getName());
        set(ItemTable.COLUMN_NAME_ATTACK, String.valueOf(item.getAttack()));
        set(ItemTable.COLUMN_NAME_DEFENSE, String.valueOf(item.getDefense()));
        set(ItemTable.COLUMN_NAME_LEVEL, String.valueOf(item.getLevel()));
        set(ItemTable.COLUMN_NAME_RESTRICTION, item.getRestriction());
        set(ItemTable.COLUMN_NAME_BONUSES, storeBonuses(item.getBonuses()));
    }
    public ItemEntry(Cursor cursor) {
        super(cursor);
    }

    public void setOrder(int order) {
        set(ItemTable.COLUMN_NAME_SORTING, String.valueOf(order));
    }
    public int getOrder() {
        return getInt(ItemTable.COLUMN_NAME_SORTING);
    }
    public void changeType(Type type) {
        set(ItemTable.COLUMN_NAME_TYPE, String.valueOf(type.value()));
    }
    @Override
    public DBTable<?> getTable() {
        return TNTContract.ITEM;
    }
    @Override
    public int getItemId() {
        return (int) getId();
    }
    @Override
    public Type getType() {
        return Type.byValue(getInt(ItemTable.COLUMN_NAME_TYPE));
    }
    @Override
    public Category getCategory() {
        return Category.values()[getInt(ItemTable.COLUMN_NAME_CATEGORY)];
    }
    @Override
    public String getIcon() {
        return get(ItemTable.COLUMN_NAME_ICON);
    }
    @Override
    public String getName() {
        return get(ItemTable.COLUMN_NAME_NAME);
    }
    @Override
    public int getAttack() {
        return getInt(ItemTable.COLUMN_NAME_ATTACK);
    }
    @Override
    public int getDefense() {
        return getInt(ItemTable.COLUMN_NAME_DEFENSE);
    }
    @Override
    public int getLevel() {
        return getInt(ItemTable.COLUMN_NAME_LEVEL);
    }
    @Override
    public String getRestriction() {
        return getNotEmpty(ItemTable.COLUMN_NAME_RESTRICTION);
    }

    private List<Bonus> bonuses = null;
    @Override
    public List<Bonus> getBonuses() {
        if (bonuses == null)
            decodeBonuses(get(ItemTable.COLUMN_NAME_BONUSES));
        return bonuses;
    }
    
    private void decodeBonuses(String string) {
        try {
            bonuses = new ArrayList<Bonus>(0);
            if (string == null)
                return;
            JSONArray array = new JSONArray(string);
            for (int i = 0; i < array.length(); i++) {
                final String bonus = array.getString(i);
                bonuses.add(new Bonus() {
                    private static final long serialVersionUID = 7534048426342316404L;
                    
                    @Override
                    public String getLabel() {
                        return bonus;
                    }
                });
            }
        } catch (JSONException e) {
            Log.d("tnt.game", "JSON: " + string);
            Log.e("tnt.game", "Exception: Failed to decode JSON", e);
        }
    }
    
    private static String storeBonuses(List<Bonus> bonuses) {
        if (bonuses == null)
            return null;
        JSONArray json = new JSONArray();
        for (Bonus b : bonuses) {
            json.put(b.getLabel());
        }
        return json.toString();
    }
}
