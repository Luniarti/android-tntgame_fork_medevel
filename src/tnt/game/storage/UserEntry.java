package tnt.game.storage;

import tnt.game.storage.TNTContract.UserTable;
import android.database.Cursor;
import de.jaschastarke.android.db.AbstractStringEntry;

public class UserEntry extends AbstractStringEntry {
    private static final long serialVersionUID = -8127561372834588220L;
    
    public UserEntry() {
        super();
    }
    public UserEntry(Cursor cursor) {
        super(cursor);
    }
    public UserEntry(String name) {
        super();
        set(UserTable.COLUMN_NAME_USERNAME, name);
    }

    public TNTContract.UserTable getTable() {
        return TNTContract.USER;
    }
    
    public String getUserImg() {
        return getNotEmpty(UserTable.COLUMN_NAME_USER_IMG);
    }
    public String getName() {
        return get(UserTable.COLUMN_NAME_USERNAME);
    }
    public String getHash() {
        return getNotEmpty(UserTable.COLUMN_NAME_HASH);
    }
    public int getGuardDifficulty() {
        return getInt(UserTable.COLUMN_NAME_GUARD_DIFFICULTY);
    }
    
    public void setHash(String val) {
        set(UserTable.COLUMN_NAME_HASH, val);
    }
    public void setUserImg(String basename) {
        set(UserTable.COLUMN_NAME_USER_IMG, basename);
    }
    public void setGuardDifficulty(int difficulty) {
        set(UserTable.COLUMN_NAME_GUARD_DIFFICULTY, String.valueOf(difficulty));
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof UserEntry) {
            return ((UserEntry) o).getId() == this.getId() && ((UserEntry) o).data == this.data;
        }
        return super.equals(o);
    }
}
