package tnt.game.storage;

import java.util.Locale;

import de.jaschastarke.android.db.DBHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TNTDBHelper extends DBHelper {
    /*
     * Has to be increased if TNTContractDQL changes, but doesn't reference the class, as long as it isn't needed to
     * avoid unnecessary string building.
     */
    public static final int DATABASE_VERSION = 12;
    public static final String DATABASE_NAME = "UserStorage.db";
    
    public TNTDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        TNTContractDQL.createTables(db);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int fromVersion, int toVersion) {
        TNTContractDQL.updateTables(db, fromVersion, toVersion);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        TNTContractDQL.recreateTables(db);
    }
    
    public UserEntry findUserByName(String name) {
        Cursor c = getWritableDatabase().query(
                TNTContract.USER.getName(),
                TNTContract.USER.getColumns(),
                "LOWER(" + TNTContract.UserTable.COLUMN_NAME_USERNAME + ") LIKE ?",
                new String[]{name.toLowerCase(Locale.US)},
                null, // group
                null, // having
                null); // orderby
        return fetchOne(TNTContract.USER, c);
    }
    public ActivityEntry findActivity(UserEntry user) {
        Cursor c = getWritableDatabase().query(
                TNTContract.ACTIVITY.getName(),
                TNTContract.ACTIVITY.getColumns(),
                TNTContract.ActivityTable.COLUMN_NAME_USER_ID + " LIKE ?",
                new String[]{String.valueOf(user.getId())},
                null, // group
                null, // having
                null); // orderby
        return fetchOne(TNTContract.ACTIVITY, c);
    }
    public boolean deleteActivity(UserEntry user) {
        return getWritableDatabase().delete(TNTContract.ActivityTable.TABLE_NAME,
                TNTContract.ActivityTable.COLUMN_NAME_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())}) != 0;
    }
    public boolean deleteMessages(UserEntry user) {
        return getWritableDatabase().delete(TNTContract.MessageTable.TABLE_NAME,
                TNTContract.ActivityTable.COLUMN_NAME_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())}) != 0;
    }
    public boolean deleteInventory(UserEntry user) {
        return getWritableDatabase().delete(TNTContract.ItemTable.TABLE_NAME,
                TNTContract.ActivityTable.COLUMN_NAME_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())}) != 0;
    }

    public void removeUser(UserEntry userEntry) {
        delete(userEntry);
        deleteActivity(userEntry);
        deleteMessages(userEntry);
        deleteInventory(userEntry);
    }
    
    public ActivityHelper activityHelper(UserEntry user) {
        return new ActivityHelper(this, user);
    }
}
