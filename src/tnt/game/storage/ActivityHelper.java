package tnt.game.storage;

import tnt.game.storage.ActivityEntry.Type;

public class ActivityHelper {
    public static final int PERFORMANCE_OFFSET = 2; // An offset to compensate performance differences
    private TNTDBHelper db;
    private UserEntry user;

    public ActivityHelper(TNTDBHelper db, UserEntry user) {
        this.db = db;
        this.user = user;
    }

    protected ActivityEntry shouldBeType(ActivityEntry entry, Type type) {
        if (entry != null && entry.getType() != type) {
            db.delete(entry);
            entry = null;
        }
        return entry;
    }
    
    public ActivityEntry missionToEndIn(ActivityEntry entry, int seconds) {
        entry = shouldBeType(entry, Type.MISSION);
        if (entry != null) {
            int end = (int) ((System.currentTimeMillis() / 1000) + seconds);
            if (entry.getEnd() < (end - 150) || entry.getEnd() > (end + 150)) { // Different Entry
                db.delete(entry);
                entry = null;
            }
        }
        if (entry == null) {
            entry = prepareMissionEntry(seconds, user);
            db.insert(entry);
        }
        return entry;
    }
    public ActivityEntry huntToEndIn(ActivityEntry entry, int seconds, String opponentName) {
        entry = shouldBeType(entry, Type.HUNT);
        if (entry != null) {
            int end = (int) ((System.currentTimeMillis() / 1000) + seconds);
            if (entry.getEnd() < (end - 60) || entry.getEnd() > (end + 60)) { // Different Entry
                db.delete(entry);
                entry = null;
            } else if (opponentName != null && (entry.getName() == null || !entry.getName().equals(opponentName))) {
                entry.setName(opponentName);
                db.update(entry);
            }
        }
        if (entry == null) {
            entry = prepareHuntEntry(seconds, user);
            db.insert(entry);
        }
        return entry;
    }
    public ActivityEntry bossFightToEndIn(ActivityEntry entry, int seconds) {
        entry = shouldBeType(entry, Type.BOSSBATTLE);
        if (entry != null) {
            int end = (int) ((System.currentTimeMillis() / 1000) + seconds);
            if (entry.getEnd() < (end - 300) || entry.getEnd() > (end + 300)) { // Different Entry
                db.delete(entry);
                entry = null;
            }
        }
        if (entry == null) {
            entry = prepareMissionEntry(seconds, user);
            db.insert(entry);
        }
        return entry;
    }
    
    public static ActivityEntry prepareHuntEntry(int inEndSeconds, UserEntry user) {
        int end = (int) ((System.currentTimeMillis() / 1000) + inEndSeconds);
        int duration = ((int) Math.ceil(inEndSeconds / 3600.0)) * 3600;
        
        ActivityEntry act = new ActivityEntry(Type.HUNT);
        act.setTimer(end + PERFORMANCE_OFFSET, duration);
        act.setUser(user);
        return act;
    }
    public static ActivityEntry prepareMissionEntry(int inEndSeconds, UserEntry user) {
        int end = (int) ((System.currentTimeMillis() / 1000) + inEndSeconds);
        int duration = ((int) Math.ceil(inEndSeconds / 300.0)) * 300;
        
        ActivityEntry act = new ActivityEntry(Type.MISSION);
        act.setTimer(end + PERFORMANCE_OFFSET, duration);
        act.setUser(user);
        return act;
    }
    public static ActivityEntry prepareBossBattleEntry(int inEndSeconds, UserEntry user) {
        int end = (int) ((System.currentTimeMillis() / 1000) + inEndSeconds);
        int duration = ((int) Math.ceil(inEndSeconds / 600.0)) * 600;
        
        ActivityEntry act = new ActivityEntry(Type.BOSSBATTLE);
        act.setTimer(end + PERFORMANCE_OFFSET, duration);
        act.setUser(user);
        return act;
    }
}
