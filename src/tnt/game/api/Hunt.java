package tnt.game.api;

import java.util.List;

public interface Hunt {
    public interface Opponent {
        public int getId();
        public String getName();
    }

    public enum State {
        OPPONENT_CHOICE,
        IN_WORK,
        MISSION_ACTIVE, // Mission is also in work, but shouldn't be handled by Hunt-API
        MISSION_FINISHED, // BattleReport
        IN_WORK_FINISHED,
        IN_BOSSBATTLE;
    }
    
    /**
     * Executes an update of the current state, and catches the data associated with the new state.
     * Doing this may affect the state, as the battle report isn't a permanent state and vanishes due to fetching.
     * 
     * Uses network. To be used asynchronous.
     */
    public State fetchCurrentState() throws ConnectionException;
    
    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not IN_WORK
     */
    public Integer getTimer();

    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not IN_WORK
     */
    public String getCurrentOpponent();
    
    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not MISSION_CHOICE
     */
    public List<Opponent> getOpponents();
    
    /**
     * Requires fetchCurrentState() first
     * 
     * @return false if currentState is not IN_WORK
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public boolean abortHunt() throws ConnectionException, SessionTimeoutException;
    
    /**
     * Requires getOpponents() first
     * 
     * @param opponent
     * @param duration Hours
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public boolean startHunt(Opponent opponent, int duration) throws ConnectionException, SessionTimeoutException;
    
    public enum Reward {
        GOLD,
        EXP;
    }
    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not IN_WORK_FINISHED
     */
    public int getReward(Reward r);
}
