package tnt.game.api;

import java.io.Serializable;
import java.util.List;

public interface BattleReport extends Serializable {
    public enum Winner {
        ATTACKER,
        DEFENDER;
    }
    
    public boolean isWon();
    public Winner getWinner();
    public String getWinnerName();
    public int getGold();
    public int getExp();
    public String getAttacker();
    public String getDefener();
    public List<String> getLog();
    public String getOpponentImage();
}
