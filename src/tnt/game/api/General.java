package tnt.game.api;

import java.io.InputStream;

public interface General extends Login {
    public InputStream loadImage(String file) throws ConnectionException;
}
