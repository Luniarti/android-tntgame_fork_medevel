package tnt.game.api;

public class SessionTimeoutException extends AuthenticationException {
    private static final long serialVersionUID = -3439793603222666000L;

    public SessionTimeoutException(String detailMessage) {
        super(detailMessage);
    }
}
