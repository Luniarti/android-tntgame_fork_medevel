package tnt.game.api;

public class ConnectionException extends Exception {
    private static final long serialVersionUID = 1445811520678736693L;
    public ConnectionException() {
        super();
    }
    public ConnectionException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
    public ConnectionException(String detailMessage) {
        super(detailMessage);
    }
    public ConnectionException(Throwable throwable) {
        super(throwable);
    }
    
}
