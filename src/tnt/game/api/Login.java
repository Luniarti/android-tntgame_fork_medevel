package tnt.game.api;

import tnt.game.storage.UserEntry;

public interface Login {
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean login(UserEntry entry) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean login(UserEntry entry, String pass) throws ConnectionException;
    public User getUserAPI(UserEntry user);
    /**
     * Notify the API that the user logged out, and wrapper don't need to be keeped any longer
     */
    boolean clearUser(UserEntry user);
}
