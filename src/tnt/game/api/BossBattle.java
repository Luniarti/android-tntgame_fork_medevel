package tnt.game.api;

public interface BossBattle {
    /**
     * Requires fetch/fetchCurrentState() first
     * 
     * @return null if currentState is not IN_BOSSBATTLE
     */
    public Integer getBossTimer();
}
