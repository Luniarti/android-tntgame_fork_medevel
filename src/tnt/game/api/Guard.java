package tnt.game.api;

public interface Guard {
    public enum Difficulty {
        VERY_SIMPLE(0),
        SIMPLE(1),
        MEDIUM(2),
        HARD(3),
        VERY_HARD(4);
        
        private int val;
        private Difficulty(int v) {
            this.val = v;
        }
        public int value() {
            return val;
        }
        public static Difficulty byValue(int v) {
            for (Difficulty e : Difficulty.values()) {
                if (e.value() == v)
                    return e;
            }
            //return null;
            throw new IllegalArgumentException("No Difficulty with value " + v + " found");
        }
    }
    /**
     * Uses network. To be used asynchronous.
     */
    public int getFreeCount() throws ConnectionException;
    /**
     * Requires getFreeCount() first
     */
    public int getRegenTimeleft();
    /**
     * Requires getFreeCount() first
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public BattleReport doGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException;
    /**
     * Like doGuard(), but skips the BattleReport for performance reason.
     * 
     * Requires getFreeCount() first
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public void doSilentGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException;
}
