package tnt.game.api;

import java.io.Serializable;
import java.util.List;

public interface Missions {
    public interface MissionHeader extends Serializable {
        /*public int getPosition();*/
        public String getTitle();
        public String getDesc();
    }
    
    public interface MissionDetail extends MissionHeader {
        public int getEndurance();
        public String getDuration();
        public int getExp();
        public int getGold();
    }
    
    public enum State {
        MISSION_CHOICE,
        IN_WORK,
        HUNT_ACTIVE, // Hunt is also in work, but shouldn't be handled by Mission-API
        HUNT_FINISHED, // In work finished
        BATTLE_REPORT,
        IN_BOSSBATTLE;
    }
    
    /**
     * Executes an update of the current state, and catches the data associated with the new state.
     * Doing this may affect the state, as the battle report isn't a permanent state and vanishes due to fetching.
     * 
     * Uses network. To be used asynchronous.
     */
    public State fetchCurrentState() throws ConnectionException;
    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not IN_WORK
     */
    public Integer getTimer();

    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not MISSION_CHOICE
     */
    public List<MissionHeader> getMissions();

    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not BATTLE_REPORT
     */
    public BattleReport getBattleReport();

    /**
     * Uses network. To be used asynchronous.
     */
    public MissionDetail getMission(MissionHeader mission) throws ConnectionException;
    
    /**
     * The Mission-Parameter may be ignored, depending on the API-Implementation, then the latest getMission() is used.
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     * @return false if mission couldn't be started (due to missing endurance)
     */
    public boolean startMission(MissionDetail mission) throws ConnectionException, SessionTimeoutException;
}
