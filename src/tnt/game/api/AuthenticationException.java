package tnt.game.api;

public class AuthenticationException extends Exception {
    private static final long serialVersionUID = 4289936214189795585L;
    public AuthenticationException(String detailMessage) {
        super(detailMessage);
    }
}
