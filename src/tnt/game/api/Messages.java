package tnt.game.api;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface Messages {
    public interface MessageHeader extends Serializable {
        public int getMessageId();
        public Date getDate();
        public String getSubject();
        public boolean isUnread();
    }
    public interface Message extends MessageHeader {
        public String getContact();
        public String getContent();
    }
    public interface NewMessage {
        public String getRecipient();
        public String getSubject();
        public String getContent();
    }
    
    public enum BoxType {
        INBOX,
        OUTBOX;
        
        public static BoxType get(int ordinal) {
            for (BoxType v : values())
                if (v.ordinal() == ordinal)
                    return v;
            throw new IllegalArgumentException();
        }
    }
    

    /**
     * Uses network. To be used asynchronous.
     */
    public List<MessageHeader> getBox(BoxType type) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean deleteAll(BoxType type) throws ConnectionException, SessionTimeoutException;
    /**
     * Uses network. To be used asynchronous.
     */
    public Message readMessage(MessageHeader msg) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean deleteMessage(Message msg) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean sendMessage(NewMessage msg) throws ConnectionException;
}
