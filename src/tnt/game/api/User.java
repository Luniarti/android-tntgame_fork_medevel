package tnt.game.api;

import java.io.File;
import java.util.Date;

public interface User {
    public interface Session {
        public String getSessionId();
        public Date getSessionTimestamp();
    }
    public Session getSession();
    
    public enum Stats {
        MAIN,
        DEFENSE,
        LIFE,
        LUCK,
        UNUSED;
    }
    
    public enum MainStat {
        INTELLIGENCE,
        STRENGTH,
        AGILITY,
        ATTACK
    }
    
    public enum Money {
        GOLD,
        RELICTS,
        DIAS
    }
    
    public Missions getMissionsAPI();
    public Guard getGuardAPI();
    public Hunt getHuntAPI();
    public BossBattle getBossAPI();
    public Messages getMessagesAPI();
    public Inventory getInventoryAPI();
    
    public int getMoney(Money m);
    public int getStat(Stats m);
    public MainStat getMaintStat();
    public int getHealth();
    public int getLevel();
    
    public String getDisplayName();
    
    /**
     * @TODO Split Points and Rank
     */
    public String getRank();
    /**
     * @TODO Strip "Ausdauer"
     */
    public String getEndurance();
    /**
     * @TODO Strip "EXP"
     */
    public String getExpLeft();

    public boolean cacheImage(File file);

    /**
     * @return the basename of the image of that user. is used to determine if the image has been changed
     */
    public String getImage();

    /**
     * Uses network. To be used asynchronous.
     */
    public void refresh() throws ConnectionException;
    
    public boolean hasNewMessages();
    
    public int getPrice(Stats m);
    public int doStatInc(Stats m);
}
