package tnt.game.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tnt.game.storage.UserEntry;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class CurrentState {
    private static final String DEFAULT_PREFERENCES = "DEFAULT";
    private Context context;
    private Bundle extras;

    public CurrentState(Context context) {
        this.context = context;
    }
    
    public SharedPreferences getPrivatePreferences() {
        return getPrivatePreferences(DEFAULT_PREFERENCES);
    }
    public SharedPreferences getPrivatePreferences(String key) {
        return context.getSharedPreferences(key, Context.MODE_PRIVATE);
    }
    
    public Bundle extras() {
        if (extras == null)
            extras = new Bundle();
        return extras;
    }
    
    private Map<Long, UserEntry> activeUsers = new HashMap<Long, UserEntry>();
    private Map<Long, Drawable> userIcons = new HashMap<Long, Drawable>();
    private Map<Long, UserState> userStates = new HashMap<Long, UserState>();
    
    public void storeLoggedinUser(UserEntry entry) {
        activeUsers.put(entry.getId(), entry);
    }
    public void removeLoggedinUser(UserEntry entry) {
        activeUsers.remove(entry.getId());
    }
    public UserEntry getActiveUser(long id) {
        return activeUsers.get(id);
    }
    public UserEntry getActiveUser(String userName) {
        for (UserEntry entry : activeUsers.values()) {
            if (entry.getName().equalsIgnoreCase(userName))
                return entry;
        }
        return null;
    }
    public List<UserEntry> getActiveUsers() {
        return new ArrayList<UserEntry>(activeUsers.values());
    }
    public Drawable getUserIcon(UserEntry user) {
        return userIcons.get(user.getId());
    }
    public void storeLoggedinUser(UserEntry entry, Drawable icon) {
        activeUsers.put(entry.getId(), entry);
        userIcons.put(entry.getId(), icon);
    }
    
    public UserState getUserState(UserEntry entry) {
        if (!userStates.containsKey(entry.getId()))
            userStates.put(entry.getId(), new UserState());
        return userStates.get(entry.getId());
    }
}
