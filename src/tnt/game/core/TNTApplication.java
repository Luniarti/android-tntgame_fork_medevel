package tnt.game.core;

import tnt.game.api.Login;
import android.app.Application;

public class TNTApplication extends Application {/*
    private CurrentState state;
    private WeakReference<Login> api;
    
    public CurrentState state() {
        if (state == null)
            state = new CurrentState(this);
        return state;
    }

    public Login api() {
        if (api == null || api.get() == null) {
            Login l = new AspxLogin(state());
            api = new WeakReference<Login>(l);
            return l;
        }
        return api.get();
    }*/
    
    public CurrentState state() {
        return container().state();
    }
    public Login api() {
        return container().api();
    }
    
    public Container container() {
        return Container.getForContext(getApplicationContext());
    }
    
    /*
    private Map<Object, Void> cache = new WeakHashMap<Object, Void>();
    public void cacheInstance(Object inst) {
        for (Iterator<Map.Entry<Object, Void>> iterator = cache.entrySet().iterator(); iterator.hasNext();) {
            if (inst.getClass().isInstance(iterator.next().getKey()))
                iterator.remove();
        }
        cache.put(inst, null);
    }
    @SuppressWarnings("unchecked")
    public <T> T getCachedInstance(Class<T> cls) {
        for (Object o : cache.keySet()) {
            if (cls.isInstance(o))
                return (T) o;
        }
        return null;
    }*/
}
