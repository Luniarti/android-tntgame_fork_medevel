package tnt.game.core;

import tnt.game.guard.GuardExecuteTask;

public class UserState {
    private GuardExecuteTask runningGuardTask;

    public GuardExecuteTask getRunningGuardTask() {
        return runningGuardTask;
    }

    public void setRunningGuardTask(GuardExecuteTask runningGuardTask) {
        this.runningGuardTask = runningGuardTask;
    }
}
