package tnt.game.core;

public class Fragment extends android.support.v4.app.Fragment {
    public TNTApplication application() {
        return (TNTApplication) getActivity().getApplication();
    }
    public Container container() {
        return Container.getForContext(getActivity().getApplicationContext());
    }
}
