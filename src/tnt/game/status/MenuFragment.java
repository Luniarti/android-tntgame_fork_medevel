package tnt.game.status;

import tnt.game.GuardActivity;
import tnt.game.HuntActivity;
import tnt.game.InventoryActivity;
import tnt.game.MessageActivity;
import tnt.game.MissionsActivity;
import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.core.Activity;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MenuFragment extends ListFragment {
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //menu = ((ListView) getView().findViewById(R.id.menu_list));
        String[] menuItems = {
            getString(R.string.missions),
            getString(R.string.guard),
            getString(R.string.hunt),
            getString(R.string.messages),
            getString(R.string.inventory)
            //,"TEST"
        };
        // We need to use a different list item layout for devices older than Honeycomb
        int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), layout, menuItems){
            @Override
            public boolean areAllItemsEnabled() {
                return true;
            }
            @Override
            public boolean isEnabled(int position) {
                return true;
            }
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (isEnabled(position))
                    convertView = null;
                TextView v = (TextView) super.getView(position, convertView, parent);
                if (!isEnabled(position))
                    v.setTextColor(getResources().getColor(R.color.bright_foreground_dark_disabled));
                return v;
            }
        };
        setListAdapter(adapter);
    }
    
    protected StatusActivity activity() {
        return (StatusActivity) getActivity();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        switch (position) {
            case 0:
                Intent mIntent = new Intent(getActivity(), MissionsActivity.class);
                mIntent.putExtra(Activity.EXTRA_USER_ENTRY, activity().user());
                startActivity(mIntent);
                break;
            case 1:
                Intent tgIntent = new Intent(getActivity(), GuardActivity.class);
                tgIntent.putExtra(Activity.EXTRA_USER_ENTRY, activity().user());
                startActivity(tgIntent);
                break;
            case 2:
                Intent hIntent = new Intent(getActivity(), HuntActivity.class);
                hIntent.putExtra(Activity.EXTRA_USER_ENTRY, activity().user());
                startActivity(hIntent);
                break;
            case 3:
                Intent msgIntent = new Intent(getActivity(), MessageActivity.class);
                msgIntent.putExtra(Activity.EXTRA_USER_ENTRY, activity().user());
                startActivity(msgIntent);
                break;
            case 4:
                Intent iIntent = new Intent(getActivity(), InventoryActivity.class);
                iIntent.putExtra(Activity.EXTRA_USER_ENTRY, activity().user());
                startActivity(iIntent);
                break;
            case 5:
                /*MissionEntry mission = activity().container().db().findMission(activity().user());
                if (mission != null)
                    activity().container().db().delete(mission);
                
                int end = (int) (System.currentTimeMillis() / 1000 + 20);
                
                mission = new MissionEntry();
                mission.setUser(activity().user());
                mission.setTimer(end, 30);
                mission.setName("Test");
                activity().container().db().insert(mission);
                activity().container().db().close();
                MissionEndReceiver.setAlarm(getActivity(), end);*/
                break;
        }
    }
}
