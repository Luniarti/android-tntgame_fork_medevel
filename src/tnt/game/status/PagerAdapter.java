package tnt.game.status;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar.TabListener;

import tnt.game.R;
import tnt.game.core.TabFragmentPagerAdapter;
import android.support.v4.app.Fragment;

public class PagerAdapter extends TabFragmentPagerAdapter implements TabListener {
    private static final int DEFAULT_PAGE = 1;
    private Fragment[] pages = new Fragment[3];
    private SherlockFragmentActivity act;
    
    public PagerAdapter(SherlockFragmentActivity act) {
        super(act.getSupportFragmentManager());
        this.act = act;
    }

    @Override
    public Fragment getItem(int idx) {
        if (pages[idx] == null) {
            switch (idx) {
                case 0:
                    pages[idx] = new StatsFragment();
                    break;
                case 1:
                    pages[idx] = new MainFragment();
                    break;
                case 2:
                    pages[idx] = new ImageFragment();
                    break;
            }
        }
        return pages[idx];
    }

    public void doRefresh() {
        if (pages[0] != null && pages[0].getView() != null)
            ((StatsFragment) pages[0]).draw();
        if (pages[2] != null && pages[2].getView() != null)
            ((ImageFragment) pages[2]).draw();
    }

    @Override
    public int getCount() {
        return pages.length;
    }

    @Override
    public String getPageTitle(int position) {
        switch (position) {
            case 0:
                return act.getString(R.string.status_title);
            case 1:
                return act.getString(R.string.main_menu);
            case 2:
                return act.getString(R.string.status_image);
            default:
                CharSequence cs = super.getPageTitle(position);
                return cs != null ? cs.toString() : null;
        }
    }

    @Override
    protected int getDefaultPage() {
        return DEFAULT_PAGE;
    }
}
