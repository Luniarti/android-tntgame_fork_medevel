package tnt.game.login;

import tnt.game.core.Activity;
import tnt.game.storage.UserEntry;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class LogoutReceiver extends BroadcastReceiver {
    public static final String LOGOUT_ACTION = "tnt.game.login.ACTION_LOGOUT";
    public static final IntentFilter FILTER = new IntentFilter();
    {
        FILTER.addAction(LOGOUT_ACTION);
    }
    
    private Activity act;
    
    public LogoutReceiver(Activity act) {
        this.act = act;
    }
    
    public boolean onLogout() {
        return true;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(Activity.EXTRA_USER_ID)) {
            UserEntry actUser = act.user();
            if (actUser != null && actUser.getId() == intent.getLongExtra(Activity.EXTRA_USER_ID, -1))
                return;
        }
        if (onLogout())
            act.finish();
    }
}
