package tnt.game.inventory;

import java.util.List;

import tnt.game.InventoryActivity;
import tnt.game.api.ConnectionException;
import tnt.game.api.Inventory.Item;
import tnt.game.api.Inventory.Type;
import tnt.game.storage.ItemEntry;
import tnt.game.storage.TNTContract;
import tnt.game.storage.TNTContract.ItemTable;
import tnt.game.storage.TNTDBHelper;
import tnt.game.storage.UserEntry;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class UpdateInventoryTask extends AsyncTask<Type, Void, Boolean> {
    private InventoryActivity context;
    private UserEntry user;

    public UpdateInventoryTask(InventoryActivity context) {
        this.context = context;
        this.user = context.user();
    }
    
    @Override
    protected Boolean doInBackground(Type... params) {
        Type type = params[0];
        
        Item[] actualItems;
        try {
            List<Item> actualItemlist = context.userApi().getInventoryAPI().fetchInventory(type);
            actualItems = actualItemlist.toArray(new Item[actualItemlist.size()]);
        } catch (ConnectionException e) {
            return false;
        }
        TNTDBHelper dbh = context.container().db();
        synchronized (dbh) {
            SQLiteDatabase db = dbh.getWritableDatabase();
            db.beginTransaction();
            Cursor query = db.query(ItemTable.TABLE_NAME,
                    ItemTable.COLUMNS,
                    ItemTable.COLUMN_NAME_USER_ID + " = ? AND " +
                            ItemTable.COLUMN_NAME_TYPE + " = ?",
                    new String[]{String.valueOf(user.getId()), String.valueOf(type.ordinal())},
                    null,
                    null,
                    null);
            db.endTransaction();
            while (query.moveToNext()) {
                ItemEntry item = TNTContract.ITEM.createEntry(query);
                int foundItem = findItem(actualItems, item.getItemId());
                if (foundItem == -1) {
                    dbh.delete(item);
                } else {
                    if (foundItem != item.getOrder()) {
                        item.setOrder(foundItem);
                        dbh.update(item);
                    }
                    actualItems[foundItem] = null;
                }
            }
            query.close();
            for (int i = 0; i < actualItems.length; i++) {
                if (actualItems[i] != null) {
                    ItemEntry item = new ItemEntry(user, actualItems[i]);
                    item.setOrder(i);
                    dbh.replace(item);
                }
            }
        }
        return true;
    }
    
    private static final int findItem(Item[] list, int itemId) {
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null && list[i].getItemId() == itemId)
                return i;
        }
        return -1;
    }

}
