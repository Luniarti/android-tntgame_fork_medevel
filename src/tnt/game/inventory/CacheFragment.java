package tnt.game.inventory;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;

public class CacheFragment extends Fragment {
    private static final String TAG = "RetainFragment";
    private static final double CACHESIZE_BITMAP_VM_FRACTION = 0.125;
    private LruCache<String, Bitmap> retainedBitmapCache;

    public CacheFragment() {}

    public static CacheFragment findOrCreateRetainFragment(FragmentManager fm) {
        CacheFragment fragment = (CacheFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new CacheFragment();
            fm.beginTransaction().add(fragment, TAG).commit();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    
    public LruCache<String, Bitmap> getBitmapCache() {
        if (retainedBitmapCache == null) {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = (int) (maxMemory * CACHESIZE_BITMAP_VM_FRACTION);
            retainedBitmapCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                @SuppressLint("NewApi")
                protected int sizeOf(String key, Bitmap bitmap) {
                    int size;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                        size = bitmap.getByteCount();
                    } else {
                        size = bitmap.getRowBytes() * bitmap.getHeight();
                    }
                    // The cache size will be measured in kilobytes rather than
                    // number of items.
                    return size / 1024;
                }
            };
        }
        return retainedBitmapCache;
    }
}
