package tnt.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.http.HttpResponse;

import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.core.Activity;
import tnt.game.core.Fragment;
import tnt.game.core.TabFragmentPagerAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;
import com.tntgame.api.fetcher.AbstractFetcher;

import de.jaschastarke.android.views.WebView;
import de.jaschastarke.utils.IOUtil;

public class BattleReportActivity extends Activity {
    public static final String EXTRA_REPORT = "tnt.game.BattleReportActivity.EXTRA_REPORT";
    protected BattleReport report;
    private Fragment[] fragments = new Fragment[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shared_activity_pager);
        
        report = (BattleReport) getIntent().getSerializableExtra(EXTRA_REPORT);
        
        final PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        final ActionBar actionBar = getSupportActionBar();
        //actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        pagerAdapter.registerWithActionBar((ViewPager) findViewById(R.id.pager), actionBar);
        
        registerForCloseOnLogout();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    class PagerAdapter extends TabFragmentPagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int idx) {
            if (fragments[idx] == null) {
                switch (idx) {
                    case 0:
                        fragments[idx] = new ReportFragment();
                        break;
                    case 1:
                        fragments[idx] = new ImageFragment();
                        break;
                }
            }
            return fragments[idx];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public String getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.battlereport);
                case 1:
                    return getString(R.string.opponent);
                default:
                    CharSequence cs = super.getPageTitle(position);
                    return cs != null ? cs.toString() : null;
            }
        }
    }
    
    public static class ReportFragment extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_battlereport, container, false);
        }
        
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            
            BattleReport report = ((BattleReportActivity) getActivity()).report;
            
            ((TextView) getView().findViewById(R.id.attacker)).setText(report.getAttacker());
            ((TextView) getView().findViewById(R.id.defender)).setText(report.getDefener());
            ((TextView) getView().findViewById(R.id.winner)).setText(report.getWinnerName());
            ((TextView) getView().findViewById(R.id.gold)).setText(String.valueOf(report.getGold()));
            ((TextView) getView().findViewById(R.id.exp)).setText(String.valueOf(report.getExp()));
            
            String brurl = "http://tnt-game.com/Main/WebGame/BattleReport.aspx";
            StringBuilder log = new StringBuilder();
            log.append("<html><style type=\"text/css\">" +
                    "body {padding: 0; margin: 0; color: #cccccc; background: transparent; font-size: 0.8em;}" +
                    "p {margin: 0; padding: 0 0 0 10pt; text-indent: -10pt;}" +
                    "p + p {margin-top: 2pt}" +
                    "</style><body>");
            for (String msg : report.getLog()) {
                log.append("<p>");
                log.append(msg);
                log.append("</p>");
            }
            log.append("</body></html>");

            final WebView msgs = (WebView) getView().findViewById(R.id.messages);
            msgs.loadDataWithBaseURL(brurl, log.toString(), "text/html", null, brurl);
            msgs.setBackgroundColor(Color.TRANSPARENT);
            msgs.setOnContentVisible(new Runnable() {
                @Override
                @SuppressWarnings("deprecation")
                public void run() {
                    msgs.scrollTo(0, (int) ((msgs.getContentHeight() * msgs.getScale()) - msgs.getHeight()));
                }
            });
        }
    }
    
    private static String md5(String text) {
        try {
            StringBuilder sb = new StringBuilder();
            byte[] bytes = MessageDigest.getInstance("MD5").digest(text.getBytes("UTF-8"));
            for (byte b : bytes)
                sb.append(String.format("%02x", b));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static class ImageFragment extends Fragment {
        private AsyncTask<File, Void, Drawable> fetchTask;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.status_fragment_image, container, false);
        }
        
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            
            final String opponentImg = ((BattleReportActivity) getActivity()).report.getOpponentImage();
            final ImageView imageView = (ImageView) getView().findViewById(R.id.char_image);
            final String cachedName = md5(opponentImg) + ".png";
            fetchTask = new AsyncTask<File, Void, Drawable>(){
                @Override
                protected Drawable doInBackground(File... params) {
                    File file = new File(getActivity().getCacheDir(), cachedName);
                    try {
                        if (isCancelled())
                            return null;
                        if (!file.exists()) {
                            Log.d("tnt.game", "BattleReport: Downloading " + opponentImg);
                            file.getParentFile().mkdirs();
                            HttpResponse response = AbstractFetcher.fetchStatic(opponentImg);
                            IOUtil.copy(response.getEntity().getContent(), new FileOutputStream(file));
                        }
                        if (isCancelled())
                            return null;
                        Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                        if (isCancelled())
                            return null;
                        return new BitmapDrawable(getResources(), bitmap);
                    } catch (FileNotFoundException e) {
                        Log.e("tnt.game", "Exception: Couldn't access " + file, e);
                    } catch (ConnectionException e) {
                        Log.e("tnt.game", "Exception: Couldn't access " + opponentImg, e);
                    } catch (IllegalStateException e) {
                        Log.e("tnt.game", "Exception: Couldn't access " + file, e);
                    } catch (IOException e) {
                        Log.e("tnt.game", "Exception: Couldn't access " + file, e);
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Drawable result) {
                    if (result != null) {
                        if (getView() != null && getView().findViewById(R.id.loading_char_image) != null) {
                            getView().findViewById(R.id.loading_char_image).setVisibility(View.GONE);
                            imageView.setImageDrawable(result);
                            imageView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error_load_image), Toast.LENGTH_SHORT).show();
                    }
                }
            }.execute(new File(getActivity().getFilesDir(), opponentImg));
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            if (fetchTask != null) {
                fetchTask.cancel(false);
                fetchTask = null;
            }
        }
    }
}
