package tnt.game.guard;

import tnt.game.GuardActivity;
import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.api.Guard;
import tnt.game.api.Guard.Difficulty;
import tnt.game.api.SessionTimeoutException;
import tnt.game.core.Activity;
import tnt.game.core.Container;
import tnt.game.storage.UserEntry;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GuardExecuteService extends IntentService {
    public static final String FINISH_ACTION = "tnt.game.guard.ACTION_FINISH";
    
    public static final String EXTRA_USER = "tnt.game.guard.EXTRA_USER";
    public static final String EXTRA_REPEAT = "tnt.game.guard.EXTRA_REPEAT";
    public static final String EXTRA_DIFFICULTY = "tnt.game.guard.EXTRA_DIFFICULTY";
    public static final String EXTRA_OFFSET = "tnt.game.guard.EXTRA_OFFSET";
    
    private static int NOTIFICATION_ID_OFFSET = 34570000;
    
    public GuardExecuteService() {
        super("GuardExecuteService");
    }
    
    public static void previewNotification(Context context, UserEntry user, int progress, int max) {
        NotificationCompat.Builder notification = prepareNotification(context, user);
        notification
            .setContentText(getNotificationText(context, user, progress, max))
            .setProgress(max, progress, false);
        
        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notifyMgr.notify(NOTIFICATION_ID_OFFSET + (int) user.getId(), notification.build());
    }
    
    protected static NotificationCompat.Builder prepareNotification(Context context, UserEntry user) {
        PendingIntent nullIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(context)
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle(context.getResources().getString(R.string.app_name))
            .setOngoing(true)
            .setWhen(System.currentTimeMillis())
            .setContentIntent(nullIntent);
    }
    
    protected static String getNotificationText(Context context, UserEntry user, int progress, int max) {
        String prefix = context.getResources().getString(R.string.character) + " " + user.getName() + " - ";
        return prefix + context.getResources().getString(R.string.progress_guard, progress, max);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "GuardExecuteServiceWakelock");
        wakeLock.acquire();
        
        try {
            NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            
            int offset = intent.getIntExtra(EXTRA_OFFSET, 0);
            int repeat = intent.getIntExtra(EXTRA_REPEAT, 1);
            
            Difficulty difficulty = Difficulty.byValue(intent.getIntExtra(EXTRA_DIFFICULTY, 0));
            Container container = Container.getForContext(getApplicationContext());
            
            UserEntry user = (UserEntry) intent.getSerializableExtra(EXTRA_USER);
            Guard api = container.api().getUserAPI(user).getGuardAPI();

            NotificationCompat.Builder notification = prepareNotification(this, user)
                .setContentText(getNotificationText(this, user, offset, offset + repeat))
                .setProgress(offset + repeat, offset + 1, false);
            notifyMgr.notify(NOTIFICATION_ID_OFFSET + (int) user.getId(), notification.build());
        
            int i = 0;
            for (; i < repeat; i++) {
                try {
                    api.doSilentGuard(difficulty);
                    
                    notification
                        .setContentText(getNotificationText(this, user, offset + i + 1, offset + repeat))
                        .setProgress(offset + repeat, offset + i + 1, false);
                    
                    notifyMgr.notify(NOTIFICATION_ID_OFFSET + (int) user.getId(), notification.build());
                } catch (SessionTimeoutException e) {
                    Log.e("tnt.game", "Exception: Failed to execute TownDefense", e);
                    break;
                } catch (ConnectionException e) {
                    Log.e("tnt.game", "Exception: Failed to execute TownDefense", e);
                    break;
                }
            }
            
            Intent clickIntent = new Intent(this, GuardActivity.class);
            clickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            clickIntent.putExtra(Activity.EXTRA_USER_ID, user.getId());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) user.getId(), clickIntent, 0);

            if (i < repeat) {
                notification
                    .setSmallIcon(R.drawable.ic_stat_alerts_and_states_warning)
                    .setContentTitle(getString(R.string.error_connection_error))
                    .setContentText(getNotificationText(this, user, offset + i, offset + repeat));
            } else {
                notification
                    .setContentText(getNotificationText(this, user, offset + repeat, offset + repeat));
            }
            notification
                .setProgress(0, 0, false)
                .setAutoCancel(true)
                .setOngoing(false)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);
            
            notifyMgr.cancel(NOTIFICATION_ID_OFFSET + (int) user.getId());
            notifyMgr.notify(NOTIFICATION_ID_OFFSET + (int) user.getId(), notification.build());
            
            Intent msg = new Intent(FINISH_ACTION);
            container.broadcast(msg);
        } finally {
            wakeLock.release();
        }
    }

}
