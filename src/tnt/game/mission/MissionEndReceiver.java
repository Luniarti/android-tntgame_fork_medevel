package tnt.game.mission;

import tnt.game.PreferencesActivity;
import tnt.game.core.Container;
import tnt.game.storage.TNTContract.ActivityTable;
import tnt.game.storage.TNTDBHelper;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class MissionEndReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.getBoolean(PreferencesActivity.KEY_NOTIFICATION_ENABLED, true)) {
            Intent service = new Intent(context, NotificationService.class);
            Log.d("tnt.game", "MissionEndReceiver: Alarm received");
            startWakefulService(context, service);
        } else {
            Log.d("tnt.game", "MissionEndReceiver: Alarm received, while notification disabled");
        }
    }
    
    public static void scheduleAlarm(Context context) {
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MissionEndReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        
        TNTDBHelper dbh = Container.getForContext(context).db();
        
        synchronized (dbh) {
            SQLiteDatabase db = dbh.getReadableDatabase();
            // Fetch the next mission end to notify
            Cursor cn = db.query(ActivityTable.TABLE_NAME,
                    ActivityTable.COLUMNS,
                    ActivityTable.COLUMN_NAME_NOTIFIED + " = 0",
                    new String[]{},
                    null,
                    null,
                    ActivityTable.COLUMN_NAME_TIME_END + " ASC",
                    "1");
            if (cn.moveToFirst()) {
                int at = cn.getInt(cn.getColumnIndex(ActivityTable.COLUMN_NAME_TIME_END));
                Log.d("tnt.game", "MissionEndReceiver: Next Mission at: " + at);
                
                int seconds = at - ((int) (System.currentTimeMillis() / 1000));
                long targetRealtime = SystemClock.elapsedRealtime() + (seconds * 1000);
                
                Log.i("tnt.game", "MissionEndReceiver: scheduling Alarm: " + seconds);
                
                alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, targetRealtime, alarmIntent);
            } else {
                Log.d("tnt.game", "MissionEndReceiver: No new next Mision");
            }
            cn.close();
            dbh.close();
        }
    }
}
