package tnt.game.mission;

import tnt.game.MissionsActivity;
import tnt.game.PreferencesActivity;
import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.api.Missions;
import tnt.game.api.Missions.MissionDetail;
import tnt.game.api.Missions.MissionHeader;
import tnt.game.api.SessionTimeoutException;
import tnt.game.core.Fragment;
import tnt.game.storage.ActivityEntry;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class DetailFragment extends Fragment {
    private static final String MISSION_STATE = "MISSION_STATE";
    private static final String MISSION_SELECT = "MISSION_SELECT";
    private MissionDetail mission;
    private MissionHeader selectedMission;
    private AsyncTask<Void, Void, MissionDetail> task;
    
    protected MissionsActivity act() {
        return (MissionsActivity) getActivity();
    }
    protected Missions api() {
        return act().userApi().getMissionsAPI();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mission_fragment_detail, container, false);
        
        if (savedInstanceState != null) {
            mission = (MissionDetail) savedInstanceState.getSerializable(MISSION_STATE);
            selectedMission = (MissionHeader) savedInstanceState.getSerializable(MISSION_SELECT);
            if (selectedMission != null) {
                updateArticleView(selectedMission);
            }
        }
        if (mission != null) {
            showMission(mission, v);
        } else {
            startUpdate();
            loading(v, true);
        }
        
        v.findViewById(R.id.mission_accept_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loading(getView(), true);
                new AsyncTask<Void, Void, ActivityEntry>() {
                    private boolean error = false;
                    @Override
                    protected ActivityEntry doInBackground(Void... params) {
                        if (act() == null || act().user() == null)
                            return null;
                        try {
                            if (!api().startMission(mission))
                                return null;
                        } catch (SessionTimeoutException e) {
                            error = true;
                            return null;
                        } catch (ConnectionException e) {
                            error = true;
                            return null;
                        }
                        if (isCancelled())
                            return null;
                        
                        ActivityEntry entry;
                        synchronized (container().db()) {
                            entry = container().db().findActivity(act().user());
                            if (entry != null)
                                container().db().delete(entry);
                            
                            int timer = api().getTimer();
                            int end = (int) ((System.currentTimeMillis() / 1000) + timer);
                            int duration = ((int) Math.max(1, Math.round(timer / 300.0))) * 300;
                            
                            entry = new ActivityEntry(ActivityEntry.Type.MISSION);
                            entry.setUser(act().user());
                            entry.setTimer(end + 2, duration); // Add an offset to compensate performance differences
                            entry.setName(mission.getTitle());
                            
                            container().db().insert(entry);
                            container().db().close();
                        }
                            
                        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        if (pref.getBoolean(PreferencesActivity.KEY_NOTIFICATION_ENABLED, true)) {
                            MissionEndReceiver.scheduleAlarm(getActivity());
                        }
                        
                        return entry;
                    }
                    @Override
                    protected void onPostExecute(ActivityEntry result) {
                        if (act() != null && getView() != null) {
                            if (error) {
                                Toast.makeText(getActivity(), getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                                getActivity().finish();
                            } else if (result != null) {
                                act().displayInWork(result);
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.error_not_enough_endurance), Toast.LENGTH_SHORT).show();
                                loading(getView(), false);
                            }
                        }
                    }
                }.execute();
            }
        });
        return v;
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(MISSION_STATE, mission);
        outState.putSerializable(MISSION_SELECT, selectedMission);
        super.onSaveInstanceState(outState);
    }

    public void updateArticleView(final MissionHeader item) {
        mission = null;
        selectedMission = item;
        if (getView() != null) {
            loading(getView(), true);
            startUpdate();
        }
    }
    
    protected void startUpdate() {
        task = new AsyncTask<Void, Void, MissionDetail>() {
            @Override
            protected MissionDetail doInBackground(Void... params) {
                try {
                    return api().getMission(selectedMission);
                } catch (ConnectionException e) {
                    return null;
                }
            }
            @Override
            protected void onPostExecute(MissionDetail result) {
                if (result == null && getActivity() != null) {
                    Toast.makeText(getActivity(), getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    return;
                }
                selectedMission = null;
                mission = result;
                if (getView() != null) {
                    showMission(result, getView());
                    loading(getView(), false);
                }
            }
        }.execute();
    }

    private void showMission(MissionDetail result, View v) {
        ((TextView) v.findViewById(R.id.mission_detail)).setText(result.getDesc());
        ((TextView) v.findViewById(R.id.mission_duration)).setText(String.valueOf(result.getDuration()));
        ((TextView) v.findViewById(R.id.mission_endurance)).setText(String.valueOf(result.getEndurance()));
        ((TextView) v.findViewById(R.id.mission_reward_exp)).setText(String.valueOf(result.getExp()));
        ((TextView) v.findViewById(R.id.mission_reward_gold)).setText(String.valueOf(result.getGold()));
        act().setTitle(result.getTitle());
        act().getSupportActionBar().setTitle(result.getTitle());
    }
    
    protected void loading(View v, boolean show) {
        if (show) {
            v.findViewById(R.id.loading_spinner).setVisibility(View.VISIBLE);
            v.findViewById(R.id.content).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
            v.findViewById(R.id.content).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (task != null)
            task.cancel(false);
    }
}
