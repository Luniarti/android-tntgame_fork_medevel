package tnt.game.mission;

import java.util.List;

import tnt.game.R;
import tnt.game.api.Missions.MissionHeader;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class MissionAdapter extends BaseAdapter implements ListAdapter {
    private LayoutInflater inflater;
    private List<MissionHeader> entries;

    public MissionAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount() {
        return entries == null ? 0 : entries.size();
    }

    public MissionHeader getItem(int position) {
        return entries.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return createView(position, convertView, parent);
    }
    public void setMissionList(List<MissionHeader> list) {
        this.entries = list;
        notifyDataSetChanged();
    }
    

    private View createView(int position, View convertView, ViewGroup parent) {
        int resource = R.layout.mission_list_item;
        View view;

        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        MissionHeader item = getItem(position);
        ((TextView) view.findViewById(R.id.menu_item_title)).setText(item.getTitle());
        ((TextView) view.findViewById(R.id.menu_item_summary)).setText(item.getDesc());

        return view;
    }
}
