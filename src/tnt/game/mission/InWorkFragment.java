package tnt.game.mission;

import java.util.Timer;
import java.util.TimerTask;

import tnt.game.MissionsActivity;
import tnt.game.R;
import tnt.game.storage.ActivityEntry;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class InWorkFragment extends Fragment {
    protected static final String TIME_FORMAT = "%02d:%02d:%02d";
    private ActivityEntry entry;
    private CountDownTimer timer;
    private Timer finishedTimer;
    
    protected MissionsActivity activity() {
        return ((MissionsActivity) getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mission_fragment_inwork, container, false);
        if (savedInstanceState != null) {
            entry = (ActivityEntry) savedInstanceState.getSerializable(MissionsActivity.EXTRA_MISSION_ENTRY);
            Log.d("tnt.game", "InWork: ReCreated from SavedInstanceState. MissionEntry: " + entry);
        } else if (getArguments() != null) {
            entry = (ActivityEntry) getArguments().getSerializable(MissionsActivity.EXTRA_MISSION_ENTRY);
            Log.d("tnt.game", "InWork: Create with Argument. MissionEntry: " + entry);
        } else if (entry != null) {
            Log.d("tnt.game", "InWork: Create with already started Timer: " + entry);
        }
        if (entry.getEnd() > time()) {
            showTimeLeft(getHolder(v), entry.getEnd() - time());
        }
        if (entry.getName() != null && entry.getName().length() > 0)
            activity().setTitle(entry.getName());
        return v;
    }
    
    private static int time() {
        return (int) (System.currentTimeMillis() / 1000);
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d("tnt.game", "InWork: Saving InstanceState. MissionEntry: " + entry);
        outState.putSerializable(MissionsActivity.EXTRA_MISSION_ENTRY, entry);
        super.onSaveInstanceState(outState);
    }

    public void startTimer(ActivityEntry newEntry) {
        if (timer != null)
            timer.cancel();
        
        entry = newEntry;
        
        Log.d("tnt.game", "InWork: Starting Timer from API: " + entry);
        if (getView() != null) {
            showTimeLeft(getHolder(null), entry.getEnd() - time());
            start();
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (entry != null)
            start();
    }

    @Override
    public void onStop() {
        super.onStop();
        cancelTimers();
    }
    
    private void cancelTimers() {
        if (timer != null)
            timer.cancel();
        timer = null;
        if (finishedTimer != null)
            finishedTimer.cancel();
        finishedTimer = null;
    }
    
    private void start() {
        cancelTimers();
        Log.d("tnt.game", "InWork: Starting timer for Entry: " + entry);
        int timeleft = entry.getEnd() - time();
        if (timeleft <= 0) {
            showTimeLeft(getHolder(null), 0);
            finishedTimer = new Timer();
            finishedTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (getActivity() != null && isResumed())
                        activity().inWorkFinished();
                }
            }, 500);
        } else {
            timer = new CountDownTimer(timeleft * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    //Log.d("tnt.game", "InWork: Tick: " + (millisUntilFinished / 1000));
                    showTimeLeft(getHolder(null), (int) (millisUntilFinished / 1000));
                }
                
                @Override
                public void onFinish() {
                    if (getActivity() != null && isResumed())
                        activity().inWorkFinished();
                }
            }.start();
        }
    }
    
    protected void showTimeLeft(Holder holder, int t) {
        if (holder != null) {
            int h = (int) Math.floor(t / 3600);
            int m = (int) Math.floor((t % 3600) / 60);
            int s = t % 60;
            holder.time_left.setText(String.format(TIME_FORMAT, h, m, s));
            holder.timer_bar.setProgress(entry.getDuration() - t);
            holder.timer_bar.setMax(entry.getDuration());
        }
    }
    
    private static class Holder {
        TextView time_left;
        ProgressBar timer_bar;
    }
    private Holder getHolder(View v) {
        if (v == null)
            v = getView();
        if (v != null) {
            if (v.getTag() != null) {
                return (Holder) v.getTag();
            } else {
                Holder h = new Holder();
                h.time_left = (TextView) v.findViewById(R.id.time_left);
                h.timer_bar = (ProgressBar) v.findViewById(R.id.time_bar);
                v.setTag(h);
                return h;
            }
        }
        return null;
    }
}
