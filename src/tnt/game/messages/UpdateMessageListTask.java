package tnt.game.messages;

import java.util.List;

import tnt.game.MessageActivity;
import tnt.game.api.ConnectionException;
import tnt.game.api.Messages;
import tnt.game.api.Messages.BoxType;
import tnt.game.api.Messages.MessageHeader;
import tnt.game.storage.MessageEntry;
import tnt.game.storage.TNTContract;
import tnt.game.storage.TNTDBHelper;
import tnt.game.storage.UserEntry;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class UpdateMessageListTask extends AsyncTask<Messages.BoxType, Void, Boolean> {
    private MessageActivity context;
    private UserEntry user;

    public UpdateMessageListTask(MessageActivity context) {
        this.context = context;
        this.user = context.user();
    }

    @Override
    protected Boolean doInBackground(BoxType... params) {
        BoxType box = params[0];
        Messages api = context.userApi().getMessagesAPI();
        
        TNTDBHelper dbh = context.container().db();
        
        try {
            List<MessageHeader> boxMessages = api.getBox(box);
            
            SQLiteDatabase db = dbh.getReadableDatabase();
            Cursor currentMsgs = db.query(TNTContract.MessageTable.TABLE_NAME,
                    TNTContract.MessageTable.COLUMNS,
                    TNTContract.MessageTable.COLUMN_NAME_USER_ID + " = ? AND " +
                            TNTContract.MessageTable.COLUMN_NAME_TYPE + " = ?",
                    new String[]{String.valueOf(user.getId()), String.valueOf(box.ordinal())},
                    null, null, null);
            
            while (currentMsgs.moveToNext()) {
                MessageEntry dbMsg = TNTContract.MESSAGE.createEntry(currentMsgs);
                MessageHeader boxMsg = findMsgById(boxMessages, (int) dbMsg.getMessageId());
                if (boxMsg == null) {
                    // delete old msg
                    dbh.delete(dbMsg);
                } else {
                    if (boxMsg.isUnread() != dbMsg.isUnread()) {
                        // Update Unread-State of already existing Message
                        dbMsg.setUnread(boxMsg.isUnread());
                        dbh.update(dbMsg);
                    }
                    // Don't insert already known message
                    boxMessages.remove(boxMsg);
                }
            }
            currentMsgs.close();
            
            for (MessageHeader newMsg : boxMessages) {
                MessageEntry newEntry = new MessageEntry(user, newMsg, box);
                dbh.insert(newEntry);
            }
            
            return true;
        } catch (ConnectionException e) {
            return false;
        } finally {
            dbh.close();
        }
    }
    
    protected static MessageHeader findMsgById(List<MessageHeader> box, int id) {
        for (MessageHeader msg : box) {
            if (msg.getMessageId() == id)
                return msg;
        }
        return null;
    }
}
