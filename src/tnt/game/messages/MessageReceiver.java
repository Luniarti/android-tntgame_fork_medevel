package tnt.game.messages;

import tnt.game.core.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class MessageReceiver extends BroadcastReceiver {
    public static final String NEW_MESSAGE_ACTION = "tnt.game.messages.ACTION_MESSAGE_UPDATE";
    public static final IntentFilter FILTER = new IntentFilter();
    {
        FILTER.addAction(NEW_MESSAGE_ACTION);
    }
    
    private Activity act;
    
    public MessageReceiver(Activity act) {
        this.act = act;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (act.user().getId() == intent.getLongExtra(Activity.EXTRA_USER_ID, -1)) {
            act.supportInvalidateOptionsMenu();
        }
    }
}
