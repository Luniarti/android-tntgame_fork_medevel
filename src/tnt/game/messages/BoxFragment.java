package tnt.game.messages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import tnt.game.MessageActivity;
import tnt.game.R;
import tnt.game.api.Messages;
import tnt.game.api.Messages.BoxType;
import tnt.game.storage.MessageEntry;
import tnt.game.storage.TNTContract;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import de.jaschastarke.android.db.DBLoader;

public class BoxFragment extends ListFragment 
        implements LoaderCallbacks<List<MessageEntry>>{
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm dd.MM.", Locale.US);
    public static final String ARGS_BOX_TYPE = "ARGS_BOX_TYPE";
    private Adapter adapter;
    private BoxType boxtype;
    private AsyncTask<?, ?, ?> updateTask;
    private boolean initialUpdated = false;
    
    protected MessageActivity activity() {
        return (MessageActivity) getActivity();
    }
    
    protected Messages api() {
        return activity().userApi().getMessagesAPI();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.boxtype = (BoxType) getArguments().getSerializable(ARGS_BOX_TYPE);
        
        adapter = new Adapter();
        setListAdapter(adapter);
        getLoaderManager().initLoader(boxtype.ordinal(), null, this);
    }

    // Called by parent PageAdapter, when page selected
    public void onPageSelected() {
        if (!initialUpdated) {
            update(true);
            initialUpdated = true;
        }
    }
    
    public void update(boolean fullUpdate) {
        if (fullUpdate) {
            updateTask = new UpdateMessageListTask(activity()) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    activity().indicateLoading(true);
                }
    
                @Override
                protected void onPostExecute(Boolean result) {
                    super.onPostExecute(result);
                    activity().indicateLoading(false);
                    updateTask = null;
                    getLoaderManager().restartLoader(boxtype.ordinal(), null, BoxFragment.this);
                }
            }.execute(boxtype);
        } else {
            getLoaderManager().restartLoader(boxtype.ordinal(), null, BoxFragment.this);
        }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(getString(R.string.no_messages));
        if (adapter.getData() == null)
            setListShown(false);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (updateTask != null) {
            updateTask.cancel(false);
            updateTask = null;
        }
    }

    class Adapter extends BaseAdapter {
        private List<MessageEntry> msgs;

        public void replaceData(List<MessageEntry> data) {
            this.msgs = data;
            notifyDataSetChanged();
        }
        
        public List<MessageEntry> getData() {
            return msgs;
        }
        
        @Override
        public int getCount() {
            if (msgs == null)
                return 0;
            return msgs.size();
        }

        @Override
        public MessageEntry getItem(int position) {
            return msgs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return msgs.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.messages_message_entry, parent, false);
            }
            
            MessageEntry item = getItem(position);
            TextView subject = (TextView) convertView.findViewById(R.id.subject);
            subject.setText(item.getSubject());
            //((TextView) convertView.findViewById(R.id.contact)).setText(item.get);
            
            if (item.getDate() == null) {
                ((TextView) convertView.findViewById(R.id.date)).setText("");
            } else {
                //Calendar c = Calendar.getInstance();
                //c.setTime(item.getDate());
                //String date = c.get(Calendar.YEAR) != year ? DATE_FORMAT_EXTENDED.format(item.getDate()) : DATE_FORMAT.getDa
                ((TextView) convertView.findViewById(R.id.date)).setText(DATE_FORMAT.format(item.getDate()));
            }
            if (item.isUnread()) {
                //subject.setTextSize(1);
                subject.setTypeface(null, Typeface.BOLD);
                ((ImageView) convertView.findViewById(R.id.ic_unread)).setVisibility(View.VISIBLE);
            } else {
                subject.setTypeface(null, 0);
                //subject.setTextSize(0.9);
                ((ImageView) convertView.findViewById(R.id.ic_unread)).setVisibility(View.GONE);
            }
            convertView.findViewById(R.id.contact).setVisibility(View.GONE);
            
            return convertView;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        MessageEntry msg = ((Adapter) getListAdapter()).getItem(position);
        activity().viewMessage(msg);
    }

    @Override
    public Loader<List<MessageEntry>> onCreateLoader(int id, Bundle args) {
        if (BoxType.get(id) == null)
            throw new IllegalArgumentException();
        return new DBLoader<MessageEntry>(getActivity(), activity().container().db(), TNTContract.MESSAGE)
                .setWhere(TNTContract.MessageTable.COLUMN_NAME_TYPE + " = ? AND " +
                                TNTContract.MessageTable.COLUMN_NAME_USER_ID + " = ?")
                .setWhereArgs(new String[] {String.valueOf(id), String.valueOf(activity().user().getId())})
                .setOrderBy(TNTContract.MessageTable.COLUMN_NAME_MESSAGE_ID +  " DESC");
    }
    @Override
    public void onLoadFinished(Loader<List<MessageEntry>> loader, List<MessageEntry> data) {
        setListShown(true);
        adapter.replaceData(data);
        /*if (updateTask == null)
            activity().indicateLoading(false);*/
    }
    @Override
    public void onLoaderReset(Loader<List<MessageEntry>> loader) {
        adapter.replaceData(null);
    }
}
