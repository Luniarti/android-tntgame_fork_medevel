package tnt.game;

import tnt.game.api.BattleReport;
import tnt.game.api.Missions.MissionHeader;
import tnt.game.core.Activity;
import tnt.game.mission.DetailFragment;
import tnt.game.mission.InWorkFragment;
import tnt.game.mission.SelectFragment;
import tnt.game.storage.ActivityEntry;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;

public class MissionsActivity extends Activity {
    private static final String BACKSTACK_MISSION_DISPLAY = "MISSION_DISPLAY";
    public static final String EXTRA_MISSION_ENTRY = "MISSION_ENTRY";
    public static long active = 0;
    
    private DetailFragment detail;
    private SelectFragment list;
    private InWorkFragment inwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missions);

        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        if (savedInstanceState != null) {
            return;
        }
        
        list = new SelectFragment();
        getSupportFragmentManager().beginTransaction()
            .add(R.id.fragment_container, list)
            .commit();
        
        registerForCloseOnLogout();
        registerForNewMessage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void selectMission(MissionHeader item) {
        if (detail == null)
            detail = new DetailFragment();
        if (findViewById(R.id.fragment_container) != null) {
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, detail)
                .addToBackStack(BACKSTACK_MISSION_DISPLAY)
                .commit();
        }
        detail.updateArticleView(item);
        //getSupportFragmentManager().executePendingTransactions();
    }
    
    public void displayInWork(ActivityEntry timer) {
        if (inwork == null)
            inwork = new InWorkFragment();
        if (findViewById(R.id.fragment_container) != null) {
            getSupportFragmentManager().popBackStackImmediate(BACKSTACK_MISSION_DISPLAY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, inwork)
                .commit();
        }
        inwork.startTimer(timer);
    }
    
    public void inWorkFinished() {
        if (findViewById(R.id.fragment_container) != null) {
            if (list == null)
                list = new SelectFragment();
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, list)
                .commit();
        }
        list.fillMissions();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setTitle(R.string.missions);
        getSupportActionBar().setTitle(R.string.missions);
    }
    
    private BattleReport currentReport;
    public void displayBattleReportNotification(BattleReport report) {
        currentReport = report;
        View notification = findViewById(R.id.notification);
        if (notification instanceof ViewStub)
            notification = ((ViewStub) notification).inflate();
        notification.setVisibility(View.VISIBLE);
        ((TextView) notification.findViewById(R.id.notification_text)).setText(getString(R.string.battlereport));
        ((TextView) notification.findViewById(R.id.notification_subtext)).setText(getString(R.string.battle_report_winner, currentReport.getWinnerName()));
        notification.findViewById(R.id.notification_dismiss).setVisibility(View.VISIBLE);
    }
    
    public void clickNotification(View v) {
        if (currentReport != null) {
            Intent intent = new Intent(this, BattleReportActivity.class);
            intent.putExtra(BattleReportActivity.EXTRA_REPORT, currentReport);
            startActivity(intent);
        }
        v.setVisibility(View.GONE);
        currentReport = null;
    }
    public void dismissNotification(View v) {
        findViewById(R.id.notification).setVisibility(View.GONE);
        currentReport = null;
    }

    public void back() {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = 0;
    }
    @Override
    protected void onStart() {
        super.onStart();
        active = user().getId();
    }
}
