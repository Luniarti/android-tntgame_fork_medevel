package tnt.game;

import com.actionbarsherlock.view.Menu;

import tnt.game.api.ConnectionException;
import tnt.game.api.Guard;
import tnt.game.api.Guard.Difficulty;
import tnt.game.core.Activity;
import tnt.game.guard.GuardExecuteService;
import tnt.game.guard.GuardExecuteTask;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class GuardActivity extends Activity
        implements OnSeekBarChangeListener {
    
    private static final String TIME_END = "TIME_END";
    private static final String CURRENT_COUNT = "CURRENT_COUNT";
    protected static final String TIME_FORMAT = "%02d:%02d";
    
    private TextView currentCount;
    private TextView timeLeft;
    private long timeEnd;
    private CountDownTimer timer;
    private SeekBar repeatSeekBar;
    private Spinner difficultySpinner;
    private BroadcastReceiver serviceFinishReceiver;
    private GuardExecuteTask task;
    private ProgressDialog progress;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guard);
        
        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        registerForCloseOnLogout();
        registerForNewMessage();
        
        difficultySpinner = (Spinner) findViewById(R.id.difficulty);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.guard_difficulties,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        difficultySpinner.setAdapter(adapter);
        difficultySpinner.setSelection(user().getGuardDifficulty());
        
        repeatSeekBar = (SeekBar) findViewById(R.id.repeat);
        repeatSeekBar.setOnSeekBarChangeListener(this);
        
        currentCount = (TextView) findViewById(R.id.current_count);
        timeLeft = (TextView) findViewById(R.id.next_timer);
        
        if (savedInstanceState != null) {
            timeEnd = savedInstanceState.getLong(TIME_END);
            int cc = savedInstanceState.getInt(CURRENT_COUNT);
            repeatSeekBar.setMax(cc - 1);
            ((TextView) findViewById(R.id.available_count)).setText(String.valueOf(cc));
            currentCount.setText(String.valueOf(repeatSeekBar.getProgress() + 1));
            
            task = userState().getRunningGuardTask();
            if (task != null) {
                progress = prepareProgressDialog(task.getProgress(), task.getMaximum());
                progress.show();
                task.updateContext(this, progress);
                userState().setRunningGuardTask(null);
            }
        } else {
            init(true);
        }
        
        serviceFinishReceiver = container().register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                init(false);
            }
        }, new IntentFilter(GuardExecuteService.FINISH_ACTION));
    }
    
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, StatusActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_USER_ENTRY, user());
        startActivity(intent);
        return true;
    }
    
    @Override
    protected void onDestroy() {
        if (serviceFinishReceiver != null) {
            container().unregister(serviceFinishReceiver);
            serviceFinishReceiver = null;
        }
        if (task != null && task.getProgress() < task.getMaximum()) {
            task.updateContext(this, null);
            userState().setRunningGuardTask(task);
            task = null;
        }
        progress = null;
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_COUNT, repeatSeekBar.getMax() + 1);
        outState.putLong(TIME_END, timeEnd);
        super.onSaveInstanceState(outState);
    }


    private void init(final boolean closeOnError) {
        final Button button = (Button) findViewById(R.id.start_guard);
        
        currentCount.setText("1");
        repeatSeekBar.setEnabled(false);
        button.setEnabled(false);
        
        final Guard api = userApi().getGuardAPI();
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    return api.getFreeCount();
                } catch (ConnectionException e) {
                    return null;
                }
            }
            @Override
            protected void onPostExecute(Integer result) {
                if (result == null) {
                    Toast.makeText(GuardActivity.this, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }
                int c = result;
                int regenTimeleft = api.getRegenTimeleft();
                if (regenTimeleft < 0) { // We're at max
                    c++;
                }
                if (regenTimeleft > 0) {
                    timeEnd = time() + regenTimeleft;
                    displayTimer();
                } else if (regenTimeleft == 0) {
                    timeLeft.setText("...");
                } else {
                    timeLeft.setText("");
                }
                updateAvailableCount(c);
            }
        }.execute();
    }
    
    public void updateAvailableCount(int c) {
        //availCount = c;
        ((TextView) findViewById(R.id.available_count)).setText(String.valueOf(c));
        repeatSeekBar.setMax(c - 1);
        repeatSeekBar.setEnabled(c > 0);
        currentCount.setText(String.valueOf(repeatSeekBar.getProgress() + 1));
        ((Button) findViewById(R.id.start_guard)).setEnabled(c > 0);
    }
    
    protected void displayTimer() {
        timer = new CountDownTimer((timeEnd - time()) * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                showTimeLeft((int) (millisUntilFinished / 1000));
            }
            @Override
            public void onFinish() {
                timeLeft.setText("...");
            }
        }.start();
    }
    
    protected void showTimeLeft(int t) {
        int m = (int) Math.floor(t / 60);
        int s = t % 60;
        timeLeft.setText(String.format(TIME_FORMAT, m, s));
    }
    
    private static int time() {
        return (int) (System.currentTimeMillis() / 1000);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null)
            timer.cancel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (timeEnd > time())
            displayTimer();
        else
            timeLeft.setText("...");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        currentCount.setText(String.valueOf(progress + 1));
    }
    
    protected ProgressDialog prepareProgressDialog(int progressOffset, int count) {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.guard));
        //progress.setMessage(getString(R.string.please_wait));

        progress.setCanceledOnTouchOutside(false);
        progress.setProgress(progressOffset);
        progress.setMax(count);
        if (count > 1) {
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        } else {
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        }
        
        if (count > 1) {
            progress.setCancelable(true);
            progress.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (task != null)
                        task.cancel(false);
                }
            });
            progress.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.into_background), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (task != null)
                        task.sendToService();
                    finish();
                }
            });
            progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (task != null)
                        task.cancel(false);
                }
            });
        } else {
            progress.setCancelable(false);
        }
        return progress;
    }
    
    public void update() {
        if (task.getProgress() >= task.getMaximum())
            task = null;
        init(false);
    }
    
    public void startGuard(View btn) {
        int availCount = repeatSeekBar.getMax() + 1;
        int repeat = repeatSeekBar.getProgress() + 1;
        
        progress = prepareProgressDialog(0, repeat);
        
        final int difficultyVal = difficultySpinner.getSelectedItemPosition();
        Difficulty difficulty = Difficulty.byValue(difficultyVal);
        
        task = new GuardExecuteTask(this, progress, difficulty);
        task.availCount = availCount;
        
        progress.show();
        task.execute(repeat);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}
}
