package tnt.game.alwayson;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import tnt.game.PreferencesActivity;
import tnt.game.core.Container;
import tnt.game.storage.TNTContract;
import tnt.game.storage.UserEntry;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class AlwaysOnService extends Service {
	
	//privates

	private static final String HTTP_DOMAIN = "tnt-game.com";
    private static final String USERLOGIN_COOKIE = "LoginID";
    private static final int TIMEOUT = 60000;
    private static final String URI_STATUS_INFO = "http://" + 
    						HTTP_DOMAIN + "/Main/WebGame/StatusInfo.aspx";
    private static final String TAG = "TnTAlwaysOnService";
    private static final String THTAG = "TnTAlwaysOnServiceThread";
    public static final String USER_AGENT = "TnT-Game Android";
    private long mTime = ((60 * 2) + (60 / 2)) * 1000; //2 minutes and a half minute
	private Handler mHandler;
	private HandlerThread myThread;
	private Context mContext;
	
	//publics
	public static Boolean mIsServiceRunning = false;
			
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		Log.d(TAG, "onStart");	
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mContext = getApplicationContext();
		myThread = new HandlerThread(THTAG);
		
		myThread.start();
		try {
			HandlerThread.sleep(1000);
			mIsServiceRunning = true;
			mHandler = new Handler(myThread.getLooper());
			mHandler.postDelayed(doRelog, mTime);
		}
		catch (InterruptedException e){
			e.printStackTrace();
			mIsServiceRunning = false;
		}
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		mIsServiceRunning = false;
	}

	private Runnable doRelog = new Runnable() {	
		@Override
		public void run() {
			if(checkSettings()){
				for(UserEntry user: getAllUsers()){
					if (doUserRelog(user)){
						Log.d(TAG,"Relog  "+ user.getName() + " successfull!");
					} else { Log.d(TAG,"Relog  "+ user.getName() + " failed!"); }
				}
			}
			mHandler.postDelayed(this, mTime);
		}
	};
	
	private String getLoginID(UserEntry user) {
        return "Login=" + user.getName() + "&Password=" + user.getHash();
    }
	
	private boolean doUserRelog(UserEntry user){ 
		Response response;
		try {
			Connection con = Jsoup.connect(URI_STATUS_INFO)
					.timeout(TIMEOUT)
					.userAgent(USER_AGENT)
					.cookie(USERLOGIN_COOKIE, getLoginID(user))
					.followRedirects(true);
			response = con.execute();
			if (response.statusCode() != 200) {
				con.request().cookies().putAll(response.cookies());
				response = con.execute();
				if (con.response().statusCode() == 200){ return true; }
			} else { return true; }
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	private ArrayList<UserEntry> getAllUsers(){
		Container container = Container.getForContext(mContext);
		ArrayList<UserEntry> availableUsers = new ArrayList<UserEntry>();
        synchronized (container.db()) {
            Cursor query = container.db().getWritableDatabase().query(
            		TNTContract.UserTable.TABLE_NAME,
                    new String[]{
            				TNTContract.UserTable._ID, 
            				TNTContract.UserTable.COLUMN_NAME_USERNAME, 
            				TNTContract.UserTable.COLUMN_NAME_HASH
            		},
                    null, null, null, null, null);
            while (query.moveToNext()) {
                availableUsers.add(TNTContract.USER.createEntry(query));
            }
            query.close();
            container.db().close();
        }
        return availableUsers;
	}
	

	private boolean checkSettings() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(mContext);
        if (pref.getBoolean(PreferencesActivity.KEY_ALWAYS_ON, false)) {
        	if (pref.getBoolean(PreferencesActivity.KEY_ALWAYS_ON_ONLY_WIFI, true)){
				ConnectivityManager connManager = (ConnectivityManager) getSystemService(
        				Context.CONNECTIVITY_SERVICE);
        		NetworkInfo mWifi = connManager.getNetworkInfo(
        				ConnectivityManager.TYPE_WIFI);
        		if (mWifi.isConnected()) {
        		    return true;
        		} else { return false; }
        	} else { return true; }
        }
		return false;
	}
	
}
