package tnt.game;

import java.util.List;

import tnt.game.api.ConnectionException;
import tnt.game.api.Login;
import tnt.game.api.User;
import tnt.game.core.Activity;
import tnt.game.storage.TNTContract;
import tnt.game.storage.TNTDBHelper;
import tnt.game.storage.UserEntry;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
    public static final String EXTRA_ADDITIONAL_LOGIN = "tnt.game.LoginActivity.EXTRA_ADDITION_LOGIN";
    public static final String EXTRA_SWITCH_LOGIN = "tnt.game.LoginActivity.EXTRA_SWITCH_LOGIN";
    
    private static final CharSequence STORED_PW_CONST = "\0\0\0\0\0\0\0\0";
    //private static final String CURRENT_USER_PREF = "CURRENT_USER";
    //private static final String CURRENT_USER_ID = "USER_ID";
    private UserEntry loginUser;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        /*if (!getIntent().hasExtra(EXTRA_ADDITIONAL_LOGIN) && application().state().isLoggedIn()) {
            loginUser = application().state().getActiveUser();
            loading(true);
            gotoStatus();
            return;
        }*/

        final EditText u = ((EditText) findViewById(R.id.username));
        final EditText p = ((EditText) findViewById(R.id.password));
        u.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (p.getText().toString().equals(STORED_PW_CONST)) {
                    p.setText("");
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
        p.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (((EditText) v).getText().toString().equals(STORED_PW_CONST)) {
                    ((EditText) v).setText("");
                }
            }
        });
        
        if (getIntent().hasExtra(EXTRA_SWITCH_LOGIN)) {
            tryLogin(getIntent().getLongExtra(EXTRA_SWITCH_LOGIN, -1L));
            return;
        } else if (savedInstanceState != null) {
            return;
        }
        
        if (!getIntent().hasExtra(EXTRA_ADDITIONAL_LOGIN)) {
            loading(true);
            
            long lastUserId = container().state().getPrivatePreferences().getLong(PREF_LAST_USER, -1);
            if (lastUserId > 0) {
                loginUser = container().state().getActiveUser(lastUserId);
            } else {
                loginUser = null;
            }
            if (loginUser != null) {
                gotoStatus();
            } else {
                doRelogin(lastUserId); // Shows Login if no relogin available
            }
        }
        
        /*
        SharedPreferences pref = application().state().getPrivatePreferences(CURRENT_USER_PREF);
        if (pref.contains(CURRENT_USER_ID)) {
            
            new ShowStoredLoginTask(getApplicationContext())
                .setCallback(new CallbackResult<UserEntry>() {
                    public void onCallback(UserEntry result) {
                        if (result != null) {
                            loginUser = result;
                            u.setText(result.getName());
                            if (result.getHash() != null)
                                p.setText(STORED_PW_CONST);
                        }
                    }
                }).execute(pref.getLong(CURRENT_USER_ID, -1));
        }*/
    }
    
    public void tryLogin(long userId) {
        loading(true);
        new AsyncTask<Long, Void, UserEntry>() {
            private UserEntry user;
            @Override
            protected UserEntry doInBackground(Long... params) {
                user = container().db().find(TNTContract.USER, params[0]);

                boolean success = false;
                if (user != null) {
                    Log.i("tnt.game", "Login: Trylogin: " + user.getName());
                    try {
                        success = container().api().login(user);
                    } catch (ConnectionException e) {
                        Log.e("tnt.game", "Exception: Login failed", e);
                    }
                    
                    if (!success) {
                        user.setHash(null);
                    }
                    
                    container().db().update(user);
                    container().db().close();
                }
                return success ? user : null;
            }
            @Override
            protected void onPostExecute(UserEntry result) {
                if (result == null) {
                    if (user != null)
                        ((EditText) findViewById(R.id.username)).setText(user.getName());
                    else
                        ((EditText) findViewById(R.id.username)).setText("");
                    ((EditText) findViewById(R.id.password)).setText("");
                    Toast.makeText(LoginActivity.this, getString(R.string.login_invalid), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    loginUser = result;
                    gotoStatus();
                }
            }
        }.execute(userId);
    }
    
    public void doRelogin(final long lastUserId) {
        new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... params) {
                boolean success = false;
                loginUser = null;
                TNTDBHelper db = container().db();
                
                if (lastUserId > 0) {
                    loginUser = db.find(TNTContract.USER, lastUserId);
                    if (loginUser != null && (loginUser.getHash() == null || loginUser.getHash().length() == 0)) {
                        loginUser = null;
                    }
                }
                
                if (loginUser == null) {
                    List<UserEntry> users = application().state().getActiveUsers();
                    if (users.size() > 0) {
                        loginUser = users.get(users.size() - 1);
                    }
                }
                
                if (loginUser == null) {
                    Cursor query = db.getReadableDatabase().query(TNTContract.UserTable.TABLE_NAME,
                            TNTContract.UserTable.COLUMNS,
                            TNTContract.UserTable.COLUMN_NAME_HASH + " <> ''",
                            null, null, null, null);
                    if (query.moveToFirst()) {
                        loginUser = TNTContract.USER.createEntry(query);
                    }
                    query.close();
                }

                if (loginUser != null)
                    Log.i("tnt.game", "Login: Relogin: " + loginUser.getName());
                if (loginUser != null && application().state().getActiveUser(loginUser.getId()) == null) {
                    try {
                        success = container().api().login(loginUser);
                        if (!success) {
                            loginUser.setHash(null);
                            container().db().update(loginUser);
                        } else {
                            application().state().storeLoggedinUser(loginUser);
                        }
                    } catch (ConnectionException e) {
                        Log.e("tnt.game", "Exception: Login failed", e);
                    }
                }
                db.close();
                return success;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    gotoStatus();
                } else {
                    loading(false);
                }
            }
        }.execute();
    }

    public void doLogin(View view) {
        final String user = ((EditText) findViewById(R.id.username)).getText().toString();
        final String pass = ((EditText) findViewById(R.id.password)).getText().toString();
        
        if (pass.equals("")) {
            ((EditText) findViewById(R.id.password)).requestFocus();
            return;
        }
        
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr.getActiveNetworkInfo() == null || !connMgr.getActiveNetworkInfo().isConnected()) {
            Toast.makeText(this, R.string.no_network, Toast.LENGTH_LONG).show();
            return;
        }
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        
        loading(true);
        
        new AsyncTask<Login, Void, Boolean>() {
            private boolean error = false;
            @Override
            protected Boolean doInBackground(Login... params) {
                Login api = params[0];
                boolean success = false;
                try {
                    if (loginUser == null || !loginUser.getName().equals(user) || !pass.equals(STORED_PW_CONST)) {
                        loginUser = container().db().findUserByName(user);
                        if (loginUser == null) {
                            loginUser = new UserEntry(user);
                            container().db().insert(loginUser); // We need a User-Id before using the API
                        }
                        success = api.login(loginUser, pass);
                    } else {
                        success = api.login(loginUser);
                    }
                    if (success) {
                        User uApi = api.getUserAPI(loginUser);
                        if (loginUser.getName().equalsIgnoreCase(uApi.getDisplayName())) { // Fix case
                            loginUser.set(TNTContract.UserTable.COLUMN_NAME_USERNAME, uApi.getDisplayName());
                        }
                        container().db().update(loginUser);
                    } else {
                        container().db().delete(loginUser);
                    }
                    container().db().close();
                } catch (ConnectionException e) {
                    error = true;
                }
                return success;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    application().state().storeLoggedinUser(loginUser);
                    gotoStatus();
                } else if (error) {
                    loading(false);
                    Toast.makeText(LoginActivity.this, R.string.error_connection_error, Toast.LENGTH_LONG).show();
                } else {
                    loading(false);
                    Toast.makeText(LoginActivity.this, R.string.login_invalid, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(application().api());
    }
    
    private void gotoStatus() {
        Intent intent = new Intent(this, StatusActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_USER_ENTRY, loginUser);
        startActivity(intent);
    }
    
    protected void loading(boolean show) {
        if (show) {
            findViewById(R.id.loading_spinner).setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_container).setVisibility(View.GONE);
        } else {
            findViewById(R.id.loading_spinner).setVisibility(View.GONE);
            findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
        }
    }
}
